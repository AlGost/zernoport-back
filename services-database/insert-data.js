const log = require('../libs/log')(module);

module.exports = (object, values, inserter) =>  {
      let addedValues = '';
      let notAddedValues = '';
      let wrongValues = '';

      for (let el in values) {
        if (values.hasOwnProperty(el)) {
          if (inserter[el]) {
            if (inserter[el](values[el])) {
              addedValues += el + ' ';
            } else {
              notAddedValues += el + ' ';
            }
          } else {
            wrongValues += el + ' ';
          }
        }
      }
      addedValues = addedValues.substr(0, addedValues.length - 1);
      notAddedValues = notAddedValues.substr(0, notAddedValues.length - 1);
      wrongValues = wrongValues.substr(0, wrongValues.length - 1);

      return({
        successful: true,
        addedValues,
        notAddedValues,
        wrongValues
      });
};