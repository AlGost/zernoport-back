module.exports = (carrier) => {
  return {
    typeCarrier: () => {
      return carrier.typeCarrier;
    },
    whoGiving: () => {
      return carrier.passport ? carrier.passport.whoGiving : undefined;
    },
    dateGiving: () => {
      return carrier.passport ? carrier.passport.dateGiving : undefined;
    },
    codeUnit: () => {
      return carrier.passport ? carrier.passport.codeUnit : undefined;
    },
    series: () => {
      return carrier.passport ? carrier.passport.series : undefined;
    },
    passportNumbers: () => {
      return carrier.passport ? carrier.passport.numbers : undefined;
    },
    dateBirthday: () => {
      return carrier.passport ? carrier.passport.dateBirthday : undefined;
    },
    placeOfBirth: () => {
      return carrier.passport ? carrier.passport.placeOfBirth : undefined;
    },
    registration: () => {
      return carrier.passport ? carrier.passport.registration : undefined;
    },
    bankCardNumbers: () => {
      return carrier.bankCard ? carrier.bankCard.numbers : undefined;
    },
    ownerName: () => {
      return carrier.bankCard ? carrier.bankCard.ownerName : undefined;
    },
    fullName: () => {
      return carrier.requisitesOfIE ? carrier.requisitesOfIE.fullName : undefined;
    },
    INN: () => {
      return carrier.requisitesOfIE ? carrier.requisitesOfIE.INN : undefined;
    },
    OGRN: () => {
      return carrier.requisitesOfIE ? carrier.requisitesOfIE.OGRN : undefined;
    },
    checkingAccount: () => {
      return carrier.requisitesOfIE ? carrier.requisitesOfIE.checkingAccount : undefined;
    },
    corrAccount: () => {
      return carrier.requisitesOfIE ? carrier.requisitesOfIE.corrAccount : undefined;
    },
    BIK: () => {
      return carrier.requisitesOfIE ? carrier.requisitesOfIE.BIK : undefined;
    },
    bank: () => {
      return carrier.requisitesOfIE ? carrier.requisitesOfIE.bank : undefined;
    },
    marker: () => {
      return carrier.marker;
    }
  }
};