module.exports = (user) => {
  return {
    id: () => {
      return user._id;
    },
    _id: () => {
      return user._id;
    },
    firstName: () => {
      return user.firstName;
    },
    lastName: () => {
      return user.lastName;
    },
    patronymic: () => {
      return user.patronymic;
    },
    email: () => {
      return user.email;
    },
    phoneNumber: () => {
      return user.phoneNumber;
    },
    type: () => {
      return user.type;
    },
    advanced: () => {
      return user.advanced;
    },

  }
};