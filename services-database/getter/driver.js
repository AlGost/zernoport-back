module.exports = (driver) => {
  return {
    id: () => {
      return driver._id;
    },
    firstName: () => {
      return driver.firstName;
    },
    lastName: () => {
      return driver.lastName;
    },
    patronymic: () => {
      return driver.patronymic;
    },
    phoneNumber: () => {
      return driver.phoneNumber;
    },
    whoGiving: () => {
      return driver.passport ? driver.passport.whoGiving : undefined;
    },
    dateGiving: () => {
      return driver.passport ? driver.passport.dateGiving : undefined;
    },
    codeUnit: () => {
      return driver.passport ? driver.passport.codeUnit : undefined;
    },
    series: () => {
      return driver.passport ? driver.passport.series : undefined;
    },
    passportNumbers: () => {
      return driver.passport ? driver.passport.numbers : undefined;
    },
    dateBirthday: () => {
      return driver.passport ? driver.passport.dateBirthday : undefined;
    },
    placeOfBirth: () => {
      return driver.passport ? driver.passport.placeOfBirth : undefined;
    },
    registration: () => {
      return driver.passport ? driver.passport.series : undefined;
    },
    bankCardNumbers: () => {
      return driver.passport ? driver.bankCard.numbers : undefined;
    },
    ownerName: () => {
      return driver.passport ? driver.bankCard.ownerName : undefined;
    },
    car: () => {
      return driver.car;
    }
  }
};