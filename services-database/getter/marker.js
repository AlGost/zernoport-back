module.exports = (marker) => {
  return {
    id: () => {
      return marker._id;
    },
    capacity: () => {
      return marker.capacity || [];
    },
    directions: () => {
      return marker.directions || [];
    },
    typePay: () => {
      return marker.typePay || [];
    },
    dislocation: () => {
      return marker.dislocation || {};
    },
    dislocationMin: () => {
      let lng = marker.dislocation ? marker.dislocation.lng : undefined;
      let lat = marker.dislocation ? marker.dislocation.lat : undefined;
      return {
        lng,
        lat
      }
    },
    glonass: () => {
      return marker.glonass;
    },
    countCars: () => {
      return marker.countCars;
    },
    userId: () => {
      return marker.userId;
    },
    carrierId: () => {
      return marker.carrierId;
    },
    activated: () => {
      return marker.activated;
    },
    user: () => {
      let firstName = marker.user ? marker.user.firstName : undefined;
      let lastName = marker.user ? marker.user.lastName : undefined;
      let patronymic = marker.user ? marker.user.patronymic : undefined;
      let phoneNumber = marker.user ? marker.user.phoneNumber : undefined;
      return {
        firstName,
        lastName,
        patronymic,
        phoneNumber
      }
    }
  }
};