module.exports = (trailer) => {
  return {
    id: () => {
      return trailer._id;
    },
    registerSign: () => {
      return trailer.registerSign;
    },
    VIN: () => {
      return trailer.VIN;
    },
    model: () => {
      return trailer.model;
    },
    series: () => {
      return trailer.series;
    },
    numbers: () => {
      return trailer.numbers;
    },
    volume: () => {
      return trailer.volume;
    },
    car: () => {
      return trailer.car;
    }
  }
};