const carrierGetter = require('./carrier');
const carGetter = require('./car');
const driverGetter = require('./driver');
const trailerGetter = require('./trailer');
const markerGetter = require('./marker');
const userGetter = require('./user');

const Getter = function() {
  this.carrier = carrierGetter;
  this.car = carGetter;
  this.driver = driverGetter;
  this.trailer = trailerGetter;
  this.marker = markerGetter;
  this.user = userGetter;
};

module.exports = new Getter();