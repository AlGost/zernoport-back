module.exports = (car) => {
  return {
    id: () => {
      return car._id;
    },
    glonass: () => {
      return car.glonass;
    },
    VIN: () => {
      return car.VIN;
    },
    model: () => {
      return car.model;
    },
    registerSign: () => {
      return car.registerSign;
    },
    series: () => {
      return car.series;
    },
    numbers: () => {
      return car.numbers;
    },
    driver: () => {
      return car.driver;
    },
    trailer: () => {
      return car.trailer;
    }
  }
};