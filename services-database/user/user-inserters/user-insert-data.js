const insertData = require('../../insert-data');
const inserter = require('../../inserter/index');


module.exports = (user, values) => {
  return insertData(user, values, inserter.user(user));
};