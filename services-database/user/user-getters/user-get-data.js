const userGetter = require('../../getter/index').user;
const getData = require('../../get-data');
const log = require('../../../libs/log')(module);

module.exports = (user, fields) => {
  try {
    let f = fields.length !== 0 ? fields : ['id', 'firstName', 'lastName', 'patronymic', 'type'];

    let result = getData(f, userGetter(user));
    result.object.id = user._id;
    return {
      user: result.object,
      wrongValues: result.wrongValues
    }
  } catch(err) {
    log.error(err.message);
    return {
      user: {},
      wrongValues: ''
    }
  }
};