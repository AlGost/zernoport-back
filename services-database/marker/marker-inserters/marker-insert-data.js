const insertData = require('../../insert-data');
const inserter = require('../../inserter/index');


module.exports = (marker, values) => {
  return insertData(marker, values, inserter.marker(marker));
};