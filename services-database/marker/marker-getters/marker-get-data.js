const getData = require('../../get-data');
const markerGetter = require('../../getter/index').marker;
const User = require('../../../models/user');
const promiseWrap = require('../../../libs/promiseWrap');
const log = require('../../../libs/log')(module);

module.exports = async (marker, fields) => {
  try {
    let f = fields.length === 0 ? ['id', 'capacity', 'directions', 'typePay',
      'dislocation', 'glonass', 'countCars'] : fields;
    let user;

    if (f.indexOf('user') !== -1) {
      user = await promiseWrap(User.findById.bind(User), [marker.userId]);
      marker.user = user;
    }

    let result = getData(f, markerGetter(marker));
    result.object.id = marker._id;

    return {
      marker: result.object,
      wrongValues: result.wrongValues
    }
  } catch(err) {
    log.error(err.message);
    return {
      marker: {},
      wrongValues: ''
    }
  }
};