module.exports = (carrierTrailer) => {
  return {
    registerSign: (value) => {
      if (value) {
        carrierTrailer.registerSign = value;
        return true;
      }
      return false;
    },
    VIN: (value) => {
      if (value) {
        carrierTrailer.VIN = value;
        return true;
      }
      return false;
    },
    model: (value) => {
      if (value) {
        carrierTrailer.model = value;
        return true;
      }
      return false;
    },
    numbers: (value) => {
      if (value) {
        carrierTrailer.numbers = value;
        return true;
      }
      return false;
    },
    series: (value) => {
      if (value) {
        carrierTrailer.series = value;
        return true;
      }
      return false;
    },
    volume: (value) => {
      if (value) {
        carrierTrailer.volume = value;
        return true;
      }
      return false;
    },
    car: (value) => {
      if (value) {
        carrierTrailer.car = value;
        return true;
      }
      return false;
    },
  }
};