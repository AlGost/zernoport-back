module.exports = (user) => {
  return {
    firstName: (value) => {
      if (value) {
        user.firstName = value;
        return true;
      }
      return false;
    },
    lastName: (value) => {
      if (value) {
        user.lastName = value;
        return true;
      }
      return false;
    },
    patronymic: (value) => {
      if (value) {
        user.patronymic = value;
        return true;
      }
      return false;
    },
    email: (value) => {
      if (value) {
        user.email = value;
        return true;
      }
      return false;
    },
    phoneNumber: (value) => {
      if (value) {
        user.phoneNumber = value;
        return true;
      }
      return false;
    },
    type: (value) => {
      if (value) {
        user.type = value;
        return true;
      }
      return false;
    },
    password: (value) => {
      if (value) {
        user.password = value;
        return true;
      }
      return false;
    }
  };
};