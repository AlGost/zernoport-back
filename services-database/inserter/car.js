const mongoose = require('mongoose');

module.exports = (carrierCar) => {
  return {
    glonass: (value) => {
      if (value) {
        carrierCar.glonass = value;
        return true;
      }
      return false;
    },
    registerSign: (value) => {
      if (value) {
        carrierCar.registerSign = value;
        return true;
      }
      return false;
    },
    VIN: (value) => {
      if (value) {
        carrierCar.VIN = value;
        return true;
      }
      return false;
    },
    model: (value) => {
      if (value) {
        carrierCar.model = value;
        return true;
      }
      return false;
    },
    series: (value) => {
      if (value) {
        carrierCar.series = value;
        return true;
      }
      return false;
    },
    numbers: (value) => {
      if (value) {
        carrierCar.numbers = value;
        return true;
      }
      return false;
    },
    driver: (value) => {
      if (value) {
        carrierCar.driver = value;
        return true;
      }
      return false;
    },
    trailer: (value) => {
      if (value) {
        carrierCar.trailer = value;
        return true;
      }
      return false;
    }
  };
};