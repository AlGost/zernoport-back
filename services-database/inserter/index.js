const carInserter = require('./car');
const driverInserter = require('./driver');
const trailerInserter = require('./trailer');
const carrierInserter = require('./carrier');
const markerInserter = require('./marker');
const userInterter = require('./user');

const Inserter = function() {
  this.car = carInserter;
  this.driver = driverInserter;
  this.trailer = trailerInserter;
  this.carrier = carrierInserter;
  this.user = userInterter;
  this.marker = markerInserter;
};

module.exports = new Inserter();