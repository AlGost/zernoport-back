module.exports = (carrier) => {
  return {
    typeCarrier: (value) => {
      if (value) {
        carrier.typeCarrier = value;
        return true
      }
      return false;
    },
    whoGiving: (value) => {
      if (value) {
        carrier.passport = carrier.passport || {};
        carrier.passport.whoGiving = value;
        return true;
      }
      return false;
    },
    dateGiving: (value) => {
      if (value) {
        carrier.passport = carrier.passport || {};
        carrier.passport.dateGiving = +value;
        return true;
      }
      return false;
    },
    codeUnit: (value) => {
      if (value) {
        carrier.passport = carrier.passport || {};
        carrier.passport.codeUnit = value;
        return true;
      }
      return false;
    },
    series: (value) => {
      if (value) {
        carrier.passport = carrier.passport || {};
        carrier.passport.series = value;
        return true;
      }
      return false;
    },
    passportNumbers: (value) => {
      if (value) {
        carrier.passport = carrier.passport || {};
        carrier.passport.numbers = value;
        return true;
      }
      return false;
    },
    dateBirthday: (value) => {
      if (value) {
        carrier.passport = carrier.passport || {};
        carrier.passport.dateBirthday = +value;
        return true;
      }
      return false;
    },
    placeOfBirth: (value) => {
      if (value) {
        carrier.passport = carrier.passport || {};
        carrier.passport.placeOfBirth = value;
        return true;
      }
      return false;
    },
    registration: (value) => {
      if (value) {
        carrier.passport = carrier.passport || {};
        carrier.passport.registration = value;
        return true;
      }
      return false;
    },
    bankCardNumbers: (value) => {
      if (value) {
        carrier.bankCard = carrier.bankCard || {};
        carrier.bankCard.numbers = value;
        return true;
      }
      return false;
    },
    ownerName: (value) => {
      if (value) {
        carrier.bankCard = carrier.bankCard || {};
        carrier.bankCard.ownerName = value;
        return true;
      }
      return false;
    },
    fullName: (value) => {
      if (value) {
        carrier.requisitesOfIE = carrier.requisitesOfIE || {};
        carrier.requisitesOfIE.fullName = value;
        return true;
      }
      return false;
    },
    INN: (value) => {
      if (value) {
        carrier.requisitesOfIE = carrier.requisitesOfIE || {};
        carrier.requisitesOfIE.INN = value;
        return true;
      }
      return false;
    },
    OGRN: (value) => {
      if (value) {
        carrier.requisitesOfIE = carrier.requisitesOfIE || {};
        carrier.requisitesOfIE.OGRN = value;
        return true;
      }
      return false;
    },
    checkingAccount: (value) => {
      if (value) {
        carrier.requisitesOfIE = carrier.requisitesOfIE || {};
        carrier.requisitesOfIE.checkingAccount = value;
        return true;
      }
      return false;
    },
    corrAccount: (value) => {
      if (value) {
        carrier.requisitesOfIE = carrier.requisitesOfIE || {};
        carrier.requisitesOfIE.corrAccount = value;
        return true;
      }
      return false;
    },
    BIK: (value) => {
      if (value) {
        carrier.requisitesOfIE = carrier.requisitesOfIE || {};
        carrier.requisitesOfIE.BIK = value;
        return true;
      }
      return false;
    },
    bank: (value) => {
      if (value) {
        carrier.requisitesOfIE = carrier.requisitesOfIE || {};
        carrier.requisitesOfIE.bank = value;
        return true;
      }
      return false;
    }
  }
};