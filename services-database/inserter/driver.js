module.exports = (carrierDriver) => {
  return {
    firstName: (value) => {
      if (value) {
        carrierDriver.firstName = value;
        return true;
      }
      return false;
    },
    lastName: (value) => {
      if (value) {
        carrierDriver.lastName = value;
        return true;
      }
      return false;
    },
    patronymic: (value) => {
      if (value) {
        carrierDriver.patronymic = value;
        return true;
      }
      return false;
    },
    phoneNumber: (value) => {
      if (value) {
        carrierDriver.phoneNumber = value;
        return true;
      }
      return false;
    },
    whoGiving: (value) => {
      if (value) {
        carrierDriver.passport = carrierDriver.passport || {};
        carrierDriver.passport.whoGiving = value;
        return true;
      }
      return false;
    },
    dateGiving: (value) => {
      if (value) {
        carrierDriver.passport = carrierDriver.passport || {};
        carrierDriver.passport.dateGiving = +value;
        return true;
      }
      return false;
    },
    dateBirthday: (value) => {
      if (value) {
        carrierDriver.passport = carrierDriver.passport || {};
        carrierDriver.passport.dateBirthday = +value;
        return true;
      }
      return false;
    },
    codeUnit: (value) => {
      if (value) {
        carrierDriver.passport = carrierDriver.passport || {};
        carrierDriver.passport.codeUnit = value;
        return true;
      }
      return false;
    },
    series: (value) => {
      if (value) {
        carrierDriver.passport = carrierDriver.passport || {};
        carrierDriver.passport.series = value;
        return true;
      }
      return false;
    },
    passportNumbers: (value) => {
      if (value) {
        carrierDriver.passport = carrierDriver.passport || {};
        carrierDriver.passport.numbers = value;
        return true;
      }
      return false;
    },
    placeOfBirth: (value) => {
      if (value) {
        carrierDriver.passport = carrierDriver.passport || {};
        carrierDriver.passport.placeOfBirth = value;
        return true;
      }
      return false;
    },
    registration: (value) => {
      if (value) {
        carrierDriver.passport = carrierDriver.passport || {};
        carrierDriver.passport.registration = value;
        return true;
      }
      return false;
    },
    bankCardNumbers: (value) => {
      if (value) {
        carrierDriver.bankCard = carrierDriver.bankCard || {};
        carrierDriver.bankCard.numbers = value;
        return true;
      }
      return false;
    },
    ownerName: (value) => {
      if (value) {
        carrierDriver.bankCard = carrierDriver.bankCard || {};
        carrierDriver.bankCard.ownerName = value;
        return true;
      }
      return false;
    },
    car: (value) => {
      if (value) {
        carrierDriver.car = value;
        return true;
      }
      return false;
    },
  }
};