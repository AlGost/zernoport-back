module.exports = (marker) => {
  return {
    glonass: (value) => {
      if (value !== undefined && value !== null) {
        marker.glonass = value;
        return true;
      }
      return false;
    },
    typePay: (value) => {
      if (value) {
        marker.typePay = value;
        return true;
      }
      return false;
    },
    directions: (value) => {
      if (value) {
        marker.directions = value;
        return true;
      }
      return false;
    },
    capacity: (value) => {
      if (value) {
        marker.capacity = value;
        return true;
      }
      return false;
    },
    dislocation: (value) => {
      if (value) {
        marker.dislocation = value;
        return true;
      }
      return false;
    },
    countCars: (value) => {
      if (value) {
        marker.countCars = value;
        return true;
      }
      return false;
    },
    userId: (value) => {
      if (value) {
        marker.userId = value;
        return true;
      }
      return false;
    },
    carrierId: (value) => {
      if (value) {
        marker.carrierId = value;
        return true;
      }
      return false;
    }
  };
};