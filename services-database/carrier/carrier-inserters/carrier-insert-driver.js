const insertData = require('../../insert-data');
const inserter = require('../../inserter/index');
const mongoose = require('mongoose');


module.exports = (carrier, values, id) => {
  let findCar = false;
  if (mongoose.Types.ObjectId.isValid(values.car)) {
    carrier.cars.forEach((el) => {
      if (el.driver && (el.driver.toString() === carrier.drivers[id]._id.toString())) {
        el.driver = null;
      }
      if (el._id.toString() === values.car) {
        findCar = true;
        el.driver = carrier.drivers[id]._id;
      }
    });
    values.car = mongoose.Types.ObjectId(values.car);
  } else {
    values.car = null;
  }
  if (!findCar) {
    delete values.car;
  }
  return insertData(carrier.drivers[id], values, inserter.driver(carrier.drivers[id]));
};