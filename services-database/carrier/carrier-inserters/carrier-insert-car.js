const insertData = require('../../insert-data');
const inserter = require('../../inserter/index');
const mongoose = require('mongoose');
//const log = require('../libs/log')(module);

module.exports = (carrier, values, id) => {
  let findDriver = false;
  let findTrailer = false;
  if (mongoose.Types.ObjectId.isValid(values.driver)) {
    carrier.drivers.forEach((el) => {
      if (el.car && (el.car.toString() === carrier.cars[id]._id.toString())) {
        el.car = null;
      }
      if (el._id.toString() === values.driver) {
        findDriver = true;
        el.car = carrier.cars[id]._id;
      }
    });
    values.driver = mongoose.Types.ObjectId(values.driver);
  } else {
    values.driver = null;
  }
  if (!findDriver) {
    delete values.driver;
  }
  if (mongoose.Types.ObjectId.isValid(values.trailer)) {
    carrier.trailers.forEach((el) => {
      if (el.car && (el.car.toString() === carrier.cars[id]._id.toString())) {
        el.car = null;
      }
      if (el._id.toString() === values.trailer) {
        findTrailer = true;
        el.car = carrier.cars[id]._id;
      }
    });
    values.trailer = mongoose.Types.ObjectId(values.trailer);
  } else {
    values.trailer = null;
  }
  if (!findTrailer) {
    delete values.trailer;
  }
  return insertData(carrier.cars[id], values, inserter.car(carrier.cars[id]));
};