const insertData = require('../../insert-data');
const inserter = require('../../inserter/index');


module.exports = (carrier, values) => {
  return insertData(carrier, values, inserter.carrier(carrier));
};