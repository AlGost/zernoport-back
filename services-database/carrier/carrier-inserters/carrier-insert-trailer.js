const insertData = require('../../insert-data');
const inserter = require('../../inserter/index');
const mongoose = require('mongoose');

module.exports = (carrier, values, id) => {
  let findCar = false;
  if (mongoose.Types.ObjectId.isValid(values.car)) {
    carrier.cars.forEach((el) => {
      if (el.trailer && (el.trailer.toString() === carrier.trailers[id]._id.toString())) {
        el.trailer = null;
      }
      if (el._id.toString() === values.car) {
        findCar = true;
        el.trailer = carrier.trailers[id]._id;
      }
    });
    values.car = mongoose.Types.ObjectId(values.car);
  } else {
    values.car = null;
  }
  if (!findCar) {
    delete values.car;
  }
  return insertData(carrier.trailers[id], values, inserter.trailer(carrier.trailers[id]));
};