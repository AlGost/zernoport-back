const trailerGetter = require('../../getter/index').trailer;
const getData = require('../../get-data');
const log = require('../../../libs/log')(module);

module.exports = (trailer, fields) => {
  try {
    let f = fields.length !== 0 ? fields : ['registerSign', 'VIN', 'model', 'series', 'numbers', 'volume', 'car'];

    let result = getData(f, trailerGetter(trailer));
    result.object.id = trailer._id;
    return {
      trailer: result.object,
      wrongValues: result.wrongValues
    }
  } catch(err) {s
    log.error(err.message);
    return {
      trailer: {},
      wrongValues: ''
    }
  }
};