const carGetter = require('../../getter/index').car;
const getData = require('../../get-data');
const log = require('../../../libs/log')(module);

module.exports = (car, fields) => {
  try {
    let f = fields.length !== 0 ? fields : ['glonass', 'registerSign', 'VIN', 'model', 'series', 'numbers', 'driver', 'trailer'];

    let result = getData(f, carGetter(car));
    result.object.id = car._id;
    return {
      car: result.object,
      wrongValues: result.wrongValues
    }
  } catch(err) {
    log.error(err.message);
    return {
      carrier: {},
      wrongValues: ''
    }
  }
};