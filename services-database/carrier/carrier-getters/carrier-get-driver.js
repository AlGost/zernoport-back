const driverGetter = require('../../getter/index').driver;
const getData = require('../../get-data');
const log = require('../../../libs/log')(module);

module.exports = (driver, fields) => {
  try {
    let f = fields.length !== 0 ? fields : ['firstName', 'lastName', 'patronymic', 'phoneNumber', 'whoGiving', 'dateGiving',
      'codeUnit', 'series', 'passportNumbers', 'dateBirthday', 'placeOfBirth', 'registration', 'bankCardNumbers',
      'ownerName', 'car'];

    let result = getData(f, driverGetter(driver));
    result.object.id = driver._id;
    return {
      driver: result.object,
      wrongValues: result.wrongValues
    }
  } catch(err) {
    log.error(err.message);
    return {
      driver: {},
      wrongValues: ''
    }
  }
};