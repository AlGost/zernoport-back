const carrierGetter = require('../../getter/index').carrier;
const getData = require('../../get-data');
const log = require('../../../libs/log')(module);

module.exports = (carrier, fields) => {
  try {
    let f = fields.length !== 0 ? fields : ['typeCarrier', 'whoGiving', 'dateGiving', 'codeUnit', 'series',
      'passportNumbers', 'dateBirthday', 'placeOfBirth', 'registration', 'bankCardNumbers', 'ownerName',
      'fullName', 'INN', 'OGRN', 'checkingAccount', 'corrAccount', 'BIK', 'bank', 'marker'];

    let result = getData(f, carrierGetter(carrier));
    result.object.id = carrier.userId;
    return {
      carrier: result.object,
      wrongValues: result.wrongValues
    }
  } catch(err) {
    log.error(err.message);
    return {
      carrier: {},
      wrongValues: ''
    }
  }
};