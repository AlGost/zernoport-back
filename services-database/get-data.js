module.exports = (fields, getter) => {
  let object = {};
  let wrongValues = '';

  fields.forEach((el) => {
    if (getter[el]) {
      object[el] = getter[el]();
    } else {
      wrongValues += el + ' ';
    }
  });
  wrongValues = wrongValues.substr(0, wrongValues.length - 1);

  return {
    object,
    wrongValues
  }
};