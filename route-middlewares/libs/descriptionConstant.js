module.exports.WRONG_PASSWORD_LOGIN = 'Wrong password or login';
module.exports.WRONG_FORMAT_DATA = 'Wrong format data';
module.exports.INTERNAL_SERVER_ERROR = 'Internal server error';
module.exports.FORBIDDEN = "You do not have permissions";

module.exports.CARRIER_CHANGED = 'Carrier was changed';
module.exports.CARRIER_DELETED = 'Carrier was deleted';
module.exports.CARRIER_GOT = 'Carrier was given';
module.exports.CARRIERS_GOT = 'Carriers were given';

module.exports.CAR_CHANGED = 'Car was changed';
module.exports.CAR_DELETED = 'Car deleted';
module.exports.CAR_NOT_FOUND = 'This car not found';
module.exports.CARS_GOT = 'Cars were given';
module.exports.CARS_CHANGED = 'Cars was changed';

module.exports.DRIVER_CHANGED = 'Driver was changed';
module.exports.DRIVER_DELETED = 'Driver deleted';
module.exports.DRIVER_NOT_FOUND = 'This driver not found';
module.exports.DRIVER_GOT = 'Driver was given';
module.exports.DRIVERS_GOT = 'Drivers were given';
module.exports.DRIVERS_CHANGED = 'Drivers was changed';

module.exports.TRAILER_CHANGED = 'Trailer was changed';
module.exports.TRAILER_DELETED = 'Trailer deleted';
module.exports.TRAILER_NOT_FOUND = 'This trailer not found';
module.exports.TRAILER_GOT = 'Trailer was given';
module.exports.TRAILERS_GOT = 'Trailers were given';
module.exports.TRAILERS_CHANGED = 'Trailers was changed';

module.exports.USER_NOT_CARRIER = 'This user is not a carrier';
module.exports.USER_ALREADY_EXISTS = 'This user already exists';
module.exports.USER_NOT_VALIDATE = 'User is not valid';
module.exports.USER_CREATED = 'User was created';
module.exports.USER_GOT = 'User was given';
module.exports.USERS_GOT = 'User was given';
module.exports.USER_CHANGED = 'User was changed';
module.exports.USER_DELETED = 'User was deleted';
module.exports.USER_SIGN_IN = 'User sign in';
module.exports.USER_SIGN_OUT = 'User sign out';
module.exports.USER_NOT_FOUND = 'This user do not exist';
module.exports.USER_NOT_HAVE_ADVANCED = 'This user do not have a advanced information';

module.exports.MARKER_VALIDATE_FAIL = 'Marker is not valid';
module.exports.MARKER_CREATED = 'Marker was created';
module.exports.MARKER_ALREADY_EXISTS = 'This carrier already have a marker';
module.exports.MARKER_GOT = 'Markers was given';
module.exports.MARKER_NOT_FOUND = 'Marker not found';
module.exports.MARKER_CHANGED = 'Marker was changed';
module.exports.MARKERS_CHANGED = 'Markers were changed';
module.exports.MARKER_DELETED = 'Marker was deleted';
module.exports.MARKERS_GOT = 'Markers were given';
