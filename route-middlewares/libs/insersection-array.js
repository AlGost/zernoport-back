module.exports = (ar1, ar2) => {
  return ar1.filter(el1 => ar2.some(el2 => el1 === el2));
};