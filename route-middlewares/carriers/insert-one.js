const mongoose = require('mongoose');
const log = require('../../libs/log')(module);
const User = require('../../models/user');
const Carrier = require('../../models/carrier');
const promiseWrap = require('../../libs/promiseWrap');
const carrierInsertData = require('../../services-database/carrier/carrier-inserters/carrier-insert-data');
const checkObjectId = require('../libs/checkMongooseObjectId');
const ResponseJson = require('../libs/response');

const USER_NOT_FOUND = require('../libs/descriptionConstant').USER_NOT_FOUND;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_NOT_CARRIER = require('../libs/descriptionConstant').USER_NOT_CARRIER;
const CARRIER_CHANGED = require('../libs/descriptionConstant').CARRIER_CHANGED;

const ERROR_USER_NOT_FOUND = require('../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_USER_WRONG_TYPE = require('../libs/errorCodeConstant').ERROR_USER_WRONG_TYPE;

module.exports = async (req, res) => {
  try {
    let userId;
    let user;
    let carrier;
    let resultInsert;
    let rj = new ResponseJson();
    let values = Object.assign({}, req.body);

    if (!checkObjectId(req.params.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    userId = mongoose.Types.ObjectId(req.params.userId);
    user = await promiseWrap(User.findById.bind(User), [userId]);

    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    if (user.type !== 'carrier') {
      rj.setAll(400, USER_NOT_CARRIER, ERROR_USER_WRONG_TYPE);
      return res.status(rj.status).json(rj);
    }
    if (!user.advanced) {
      let c = new Carrier({
        userId: user._id
      });
      carrier = await promiseWrap(c.save.bind(c));
      user.advanced = carrier._id;
      await promiseWrap(user.save.bind(user));
    } else {
      carrier = await promiseWrap(Carrier.findById.bind(Carrier), [user.advanced]);
    }

    resultInsert = carrierInsertData(carrier, values);
    await promiseWrap(carrier.save.bind(carrier));

    rj.setDescription(CARRIER_CHANGED);
    rj.set('addedValues', resultInsert.addedValues);
    rj.set('notAddedValues', resultInsert.notAddedValues);
    rj.set('wrongValues', resultInsert.wrongValues);

    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};