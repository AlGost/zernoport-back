const mongoose = require('mongoose');
const log = require('../../../libs/log')(module);
const User = require('../../../models/user');
const Carrier = require('../../../models/carrier');
const promiseWrap = require('../../../libs/promiseWrap');
const carrierInsertDriver = require('../../../services-database/carrier/carrier-inserters/carrier-insert-driver');
const checkObjectId = require('../../libs/checkMongooseObjectId');
const isJsonString = require('../../libs/isJsonString');
const ResponseJson = require('../../libs/response');

const USER_NOT_FOUND = require('../../libs/descriptionConstant').USER_NOT_FOUND;
const WRONG_FORMAT_DATA = require('../../libs/descriptionConstant').WRONG_FORMAT_DATA;
const DRIVERS_CHANGED = require('../../libs/descriptionConstant').DRIVERS_CHANGED;
const INTERNAL_SERVER_ERROR = require('../../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_NOT_CARRIER = require('../../libs/descriptionConstant').USER_NOT_CARRIER;

const ERROR_USER_NOT_FOUND = require('../../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_WRONG_FORMAT_DATA = require('../../libs/errorCodeConstant').ERROR_WRONG_FORMAT_DATA;
const ERROR_INTERNAL_SERVER_ERROR = require('../../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_USER_WRONG_TYPE = require('../../libs/errorCodeConstant').ERROR_USER_WRONG_TYPE;

module.exports = async (req, res) => {
  try {
    let userId;
    let drivers;
    let user;
    let carrier;
    let resultInsert;
    let driversLength;
    let carrierDriversLength;
    let rj = new ResponseJson();

    let addedValues = [];
    let notAddedValues = [];
    let wrongValues = [];

    if (!checkObjectId(req.params.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    userId = mongoose.Types.ObjectId(req.params.userId);

    if ((typeof req.body.drivers === 'string') && !isJsonString(req.body.drivers)) {
      rj.setAll(400, WRONG_FORMAT_DATA, ERROR_WRONG_FORMAT_DATA);
      return res.status(rj.status).json(rj);
    }
    drivers = (typeof req.body.drivers) === 'string' ? JSON.parse(req.body.drivers) : req.body.drivers;

    user = await promiseWrap(User.findById.bind(User), [userId]);

    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    if (user.type !== 'carrier') {
      rj.setAll(400, USER_NOT_CARRIER, ERROR_USER_WRONG_TYPE);
      return res.status(rj.status).json(rj);
    }

    if (!user.advanced) {
      let c = new Carrier({
        userId: user._id
      });
      carrier = await promiseWrap(c.save.bind(c));
      user.advanced = carrier._id;
      await promiseWrap(user.save.bind(user));
    } else {
      carrier = await promiseWrap(Carrier.findById.bind(Carrier), [user.advanced]);
    }

    driversLength = drivers ? drivers.length : 0;
    carrierDriversLength = carrier.drivers ? carrier.drivers.length : 0;
    for (let i = 0, max1 = driversLength; i < max1; i += 1) {
      if (drivers[i].id) {
        let suc = false;
        for (let j = 0, max2 = carrierDriversLength; j < max2; j += 1) {
          if (drivers[i].id === carrier.drivers[j]._id.toString()) {
            delete drivers[i].id;
            resultInsert = carrierInsertDriver(carrier, drivers[i], j);
            suc = true;
          }
        }
        addedValues[i] = suc ? resultInsert.addedValues : '';
        notAddedValues[i] = suc ? resultInsert.notAddedValues : 'all';
        wrongValues[i] = suc ? resultInsert.wrongValues : 'id';
      } else {
        if (drivers) {
          carrier.drivers.push({});
          resultInsert = carrierInsertDriver(carrier, drivers[i], carrier.drivers.length - 1);
          addedValues[i] = resultInsert.addedValues;
          notAddedValues[i] = resultInsert.notAddedValues;
          wrongValues[i] = resultInsert.wrongValues;
        }
      }
    }
    await promiseWrap(carrier.save.bind(carrier));

    rj.setDescription(DRIVERS_CHANGED);
    rj.set('addedValues', addedValues);
    rj.set('notAddedValues', notAddedValues);
    rj.set('wrongValues', wrongValues);

    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }

};