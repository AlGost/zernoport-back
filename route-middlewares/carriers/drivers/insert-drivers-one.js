const mongoose = require('mongoose');
const log = require('../../../libs/log')(module);
const User = require('../../../models/user');
const Carrier = require('../../../models/carrier');
const promiseWrap = require('../../../libs/promiseWrap');
const carrierInsertDriver = require('../../../services-database/carrier/carrier-inserters/carrier-insert-driver');

module.exports = async (req, res) => {
  try {
    let userId;
    let driverId;
    let user;
    let carrier;
    let haveAccess;
    let resultInsert = {};
    let driverSearched = false;

    try {
      userId = mongoose.Types.ObjectId(req.params.userId);
    } catch (err) {
      log.error(err.message);
      return res.status(404).json({
        status: 404,
        message: 'Not found',
        description: 'This user does not exist',
        errorCode: 9
      });
    }
    try {
      driverId = mongoose.Types.ObjectId(req.params.driverId);
    } catch (err) {
      log.error(err.message);
      return res.status(404).json({
        status: 404,
        message: 'Not found',
        description: 'This driver does not exist',
        errorCode: 11
      });
    }
    haveAccess = req.user._id.toString() === userId.toString() || req.user.scope === 1023;
    if (!haveAccess) {
      return res.status(403).json({
        status: 403,
        message: 'Forbidden',
        description: 'Do not have permissions',
        errorCode: 8
      });
    }
    user = await promiseWrap(User.findById.bind(User), [userId]);
    if (!user) {
      return res.status(404).json({
        status: 404,
        message: 'Not found',
        description: 'This user does not exist',
        errorCode: 9
      });
    }
    if (!user.advanced) {
      let carrier = new Carrier({
        userId: user._id
      });
      carrier = await promiseWrap(carrier.save.bind(carrier));
      user.advanced = carrier._id;
      await promiseWrap(user.save.bind(user));
    }
    carrier = await promiseWrap(Carrier.findById.bind(Carrier), [user.advanced]);
    carrier.drivers.forEach((el, i) => {
      if (el._id.toString() === driverId.toString()) {
        let fields = {};
        Object.assign(fields, req.body);
        resultInsert = carrierInsertDriver(carrier, fields, i);
        driverSearched = true;
      }
    });
    if (!driverSearched) {
      return res.status(404).json({
        status: 404,
        message: 'Not found',
        description: 'This driver does not exist',
        errorCode: 11
      });
    }
    await promiseWrap(carrier.save.bind(carrier));
    return res.status(200).json({
      status: 200,
      message: 'OK',
      description: 'Driver changed',
      addedValues: resultInsert.addedValues,
      notAddedValues: resultInsert.notAddedValues,
      wrongValues: resultInsert.wrongValues
    });
  } catch(err) {
    log.error(err.message);
    return res.status(500).json({
      status: 500,
      message: 'Internal server error',
      description: 'Internal server error'
    });
  }
};