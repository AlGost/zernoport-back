const mongoose = require('mongoose');
const log = require('../../libs/log')(module);
const User = require('../../models/user');
const Carrier = require('../../models/carrier');
const promiseWrap = require('../../libs/promiseWrap');
const getCarrierData = require('../../services-database/carrier/carrier-getters/carrier-get-data');
const ResponseJson = require('../libs/response');
const checkObjectId = require('../libs/checkMongooseObjectId');

const USER_NOT_FOUND = require('../libs/descriptionConstant').USER_NOT_FOUND;
const USER_NOT_HAVE_ADVANCED = require('../libs/descriptionConstant').USER_NOT_HAVE_ADVANCED;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_NOT_CARRIER = require('../libs/descriptionConstant').USER_NOT_CARRIER;
const CARRIER_GOT = require('../libs/descriptionConstant').CARRIER_GOT;

const ERROR_USER_NOT_FOUND = require('../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_USER_NOT_HAVE_ADVANCED = require('../libs/errorCodeConstant').ERROR_USER_NOT_HAVE_ADVANCED;
const ERROR_USER_WRONG_TYPE = require('../libs/errorCodeConstant').ERROR_USER_WRONG_TYPE;

module.exports = async (req, res) => {
  try {
    let userId;
    let user;
    let carrier;
    let resultGet;
    let responseFields = req.query.fields ? req.query.fields.split(',') : [];
    let rj = new ResponseJson();

    if (!checkObjectId(req.params.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    userId = mongoose.Types.ObjectId(req.params.userId);
    user = await promiseWrap(User.findById.bind(User), [userId]);

    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    if (user.type !== 'carrier') {
      rj.setAll(400,USER_NOT_CARRIER, ERROR_USER_WRONG_TYPE);
      return res.status(rj.status).json(rj);
    }
    if (!user.advanced) {
      rj.setAll(400, USER_NOT_HAVE_ADVANCED, ERROR_USER_NOT_HAVE_ADVANCED);
      rj.set('carrier', {});
      return res.status(rj.status).json(rj);
    }

    carrier = await promiseWrap(Carrier.findById.bind(Carrier), [user.advanced]);
    resultGet = getCarrierData(carrier, responseFields);

    rj.setDescription(CARRIER_GOT);
    rj.set('carrier', resultGet.carrier);
    rj.set('wrongValue', resultGet.wrongValues);
    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};