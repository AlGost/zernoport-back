const mongoose = require('mongoose');
const log = require('../../../libs/log')(module);
const User = require('../../../models/user');
const Carrier = require('../../../models/carrier');
const Marker = require('../../../models/marker');
const promiseWrap = require('../../../libs/promiseWrap');
const carrierInsertMarker = require('../../../services-database/marker/marker-inserters/marker-insert-data');
const checkObjectId = require('../../libs/checkMongooseObjectId');
const isJsonString = require('../../libs/isJsonString');
const ResponseJson = require('../../libs/response');
const markerValidator = require('../../../validate').marker;

const USER_NOT_FOUND = require('../../libs/descriptionConstant').USER_NOT_FOUND;
const WRONG_FORMAT_DATA = require('../../libs/descriptionConstant').WRONG_FORMAT_DATA;
const MARKERS_CHANGED = require('../../libs/descriptionConstant').MARKERS_CHANGED;
const INTERNAL_SERVER_ERROR = require('../../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_NOT_CARRIER = require('../../libs/descriptionConstant').USER_NOT_CARRIER;

const ERROR_USER_NOT_FOUND = require('../../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_WRONG_FORMAT_DATA = require('../../libs/errorCodeConstant').ERROR_WRONG_FORMAT_DATA;
const ERROR_INTERNAL_SERVER_ERROR = require('../../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_USER_WRONG_TYPE = require('../../libs/errorCodeConstant').ERROR_USER_WRONG_TYPE;

const ADMIN  = require('../../../abac-attributes/levels').ADMIN;

module.exports = async (req, res) => {
  try {
    let userId;
    let markers;
    let user;
    let carrier;
    let marker;
    let resultInsert;
    let resultValidate;
    let markersLength;
    let carrierMarkersLength;
    let rj = new ResponseJson();

    let addedValues = [];
    let notAddedValues = [];
    let wrongValues = [];
    let invalidFields = [];

    if (!checkObjectId(req.params.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    userId = mongoose.Types.ObjectId(req.params.userId);

    if ((typeof req.body.drivers === 'string') && !isJsonString(req.body.drivers)) {
      rj.setAll(400, WRONG_FORMAT_DATA, ERROR_WRONG_FORMAT_DATA);
      return res.status(rj.status).json(rj);
    }
    markers = req.body.markers;

    user = await promiseWrap(User.findById.bind(User), [userId]);

    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    if (user.type !== 'carrier') {
      rj.setAll(400, USER_NOT_CARRIER, ERROR_USER_WRONG_TYPE);
      return res.status(rj.status).json(rj);
    }

    if (!user.advanced) {
      let c = new Carrier({
        userId: user._id
      });
      carrier = await promiseWrap(c.save.bind(c));
      user.advanced = carrier._id;
      await promiseWrap(user.save.bind(user));
    } else {
      carrier = await promiseWrap(Carrier.findById.bind(Carrier), [user.advanced]);
    }

    markersLength = markers ? markers.length : 0;

    // Мне очень за это стыдно, и перед вами тоже, человеком который разбирается в этом кхм кхм коде
    for (let i = 0, max1 = markersLength; i < max1; i += 1) {
      if (req.levelAccess > ADMIN) {
        delete markers[i].userId;
        delete markers[i].carrierId;
        delete markers[i].activated;
      }
      let markerId;
      let suc = false;
      if (markers[i].id) {
        if (checkObjectId(markers[i].id)) {
          markerId = mongoose.Types.ObjectId(markers[i].id);
          marker = await promiseWrap(Marker.findOne.bind(Marker), [{_id: markerId, carrierId: carrier._id}]);
          if (marker) {
            delete markers[i].id;
            resultInsert = carrierInsertMarker(marker, markers[i]);
            resultValidate = markerValidator(marker);
            if (resultValidate.successful) {
              await promiseWrap(marker.save.bind(marker));
              suc = true;
              invalidFields[i] = '';
            } else {
              invalidFields[i] = resultValidate.invalidFields;
            }
          }
        }
      } else {
        resultValidate = markerValidator(markers[i]);
        if (resultValidate.successful) {
          let marker = new Marker({});
          resultInsert = carrierInsertMarker(marker, markers[i]);
          marker.userId = userId;
          marker.carrierId = carrier._id;
          marker = await promiseWrap(marker.save.bind(marker));
          carrier.marker.push(marker._id);
          suc = true;
          invalidFields[i] = '';
        } else {
          invalidFields[i] = resultValidate.invalidFields;
        }
      }
      addedValues[i] = suc ? resultInsert.addedValues : '';
      notAddedValues[i] = suc ? resultInsert.notAddedValues : 'all';
      wrongValues[i] = suc ? resultInsert.wrongValues : 'id';
    }
    await promiseWrap(carrier.save.bind(carrier));

    rj.setDescription(MARKERS_CHANGED);
    rj.set('addedValues', addedValues);
    rj.set('notAddedValues', notAddedValues);
    rj.set('wrongValues', wrongValues);
    rj.set('invalidFields', invalidFields);

    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }

};