const promiseWrap = require('../../../libs/promiseWrap');
const Carrier = require('../../../models/carrier');
const Marker = require('../../../models/marker');
const User = require('../../../models/user');
const mongoose = require('mongoose');
const carrierGetMarker = require('../../../services-database/marker/marker-getters/marker-get-data');
const checkObjectId = require('../../libs/checkMongooseObjectId');
const ResponseJson = require('../../libs/response');

const USER_NOT_FOUND = require('../../libs/descriptionConstant').USER_NOT_FOUND;
const MARKERS_GOT = require('../../libs/descriptionConstant').MARKERS_GOT;
const USER_NOT_HAVE_ADVANCED = require('../../libs/descriptionConstant').USER_NOT_HAVE_ADVANCED;
const INTERNAL_SERVER_ERROR = require('../../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_NOT_CARRIER = require('../../libs/descriptionConstant').USER_NOT_CARRIER;


const ERROR_USER_NOT_FOUND = require('../../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_INTERNAL_SERVER_ERROR = require('../../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_USER_NOT_HAVE_ADVANCED = require('../../libs/errorCodeConstant').ERROR_USER_NOT_HAVE_ADVANCED;
const ERROR_USER_WRONG_TYPE = require('../../libs/errorCodeConstant').ERROR_USER_WRONG_TYPE;

module.exports = async (req, res) => {
  try {
    let userId;
    let user;
    let carrier;
    let fields = req.query.fields ? req.query.fields.split(',') : [];
    let findMarker;
    let markers = [];
    let resultGet = {};
    let wrongValues = '';
    let markersLength;
    let rj = new ResponseJson();

    if (!checkObjectId(req.params.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    userId = mongoose.Types.ObjectId(req.params.userId);
    user = await promiseWrap(User.findById.bind(User), [userId]);
    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    if (user.type !== 'carrier') {
      rj.setAll(400, USER_NOT_CARRIER, ERROR_USER_WRONG_TYPE);
      return res.status(rj.status).json(rj);
    }
    if (!user.advanced) {
      rj.setAll(400, USER_NOT_HAVE_ADVANCED, ERROR_USER_NOT_HAVE_ADVANCED);
      rj.set('markers', []);
      return res.status(rj.status).json(rj);
    }
    carrier = await promiseWrap(Carrier.findById.bind(Carrier), [user.advanced]);
    markersLength = carrier.marker ? carrier.marker.length : 0;
    for (let i = 0, max = markersLength; i < max; i += 1) {
      findMarker = await promiseWrap(Marker.findById.bind(Marker), [carrier.marker[i]]);
      resultGet = await carrierGetMarker(findMarker, fields);
      markers.push(resultGet.marker);
      wrongValues = resultGet.wrongValues;
    }

    rj.setDescription(MARKERS_GOT);
    rj.set('markers', markers);
    rj.set('wrongValue', wrongValues);
    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};