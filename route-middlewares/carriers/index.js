const getOne = require('./get-one');
const getAll = require('./get-all');
const getCarsAll = require('./cars/get-cars-all');
const getDriversAll = require('./drivers/get-drivers-all');
const getTrailersAll = require('./trailers/get-trailers-all');
const getMarkersAll = require('./markers/get-marker-all');

const insertOne = require('./insert-one');
const insertCarsOne = require('./cars/insert-cars-one');
const insertCarsAll = require('./cars/insert-cars-all');
const insertTrailersOne = require('./trailers/insert-trailers-one');
const insertTrailersAll = require('./trailers/insert-trailers-all');
const insertDriversOne = require('./drivers/insert-drivers-one');
const insertDriversAll = require('./drivers/insert-drivers-all');
const insertMarkersAll = require('./markers/put-marker-all');

const deleteOne = require('./delete-one');
const deleteCarsOne = require('./cars/delete-car-one');
const deleteDriversOne = require('./drivers/delete-driver-one');
const deleteTrailersOne = require('./trailers/delete-trailer-one');

let CarriersMiddlewares = function() {
  this.getOne = getOne;
  this.getAll = getAll;
  this.getCarsAll = getCarsAll;
  this.getDriversAll = getDriversAll;
  this.getTrailersAll = getTrailersAll;
  this.getMarkerAll = getMarkersAll;

  this.insertOne = insertOne;
  this.insertCarsOne = insertCarsOne;
  this.insertCarsAll = insertCarsAll;
  this.insertTrailersOne = insertTrailersOne;
  this.insertTrailersAll = insertTrailersAll;
  this.insertDriversOne = insertDriversOne;
  this.insertDriversAll = insertDriversAll;
  this.insertMarkersAll = insertMarkersAll;

  this.deleteOne = deleteOne;
  this.deleteCarsOne = deleteCarsOne;
  this.deleteDriversOne = deleteDriversOne;
  this.deleteTrailersOne = deleteTrailersOne;
};

let cm = new CarriersMiddlewares();

module.exports = cm;