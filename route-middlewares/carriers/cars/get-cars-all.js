const promiseWrap = require('../../../libs/promiseWrap');
const Carrier = require('../../../models/carrier');
const User = require('../../../models/user');
const mongoose = require('mongoose');
const carrierGetCar = require('../../../services-database/carrier/carrier-getters/carrier-get-car');
const checkObjectId = require('../../libs/checkMongooseObjectId');
const ResponseJson = require('../../libs/response');

const USER_NOT_FOUND = require('../../libs/descriptionConstant').USER_NOT_FOUND;
const CARS_GOT = require('../../libs/descriptionConstant').CARS_GOT;
const USER_NOT_HAVE_ADVANCED = require('../../libs/descriptionConstant').USER_NOT_HAVE_ADVANCED;
const INTERNAL_SERVER_ERROR = require('../../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_NOT_CARRIER = require('../../libs/descriptionConstant').USER_NOT_CARRIER;


const ERROR_USER_NOT_FOUND = require('../../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_INTERNAL_SERVER_ERROR = require('../../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_USER_NOT_HAVE_ADVANCED = require('../../libs/errorCodeConstant').ERROR_USER_NOT_HAVE_ADVANCED;
const ERROR_USER_WRONG_TYPE = require('../../libs/errorCodeConstant').ERROR_USER_WRONG_TYPE;

module.exports = async (req, res) => {
  try {
    let userId;
    let user;
    let carrier;
    let fields = req.query.fields ? req.query.fields.split(',') : [];
    let cars = [];
    let resultGet = {};
    let wrongValues = '';
    let rj = new ResponseJson();

    if (!checkObjectId(req.params.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    userId = mongoose.Types.ObjectId(req.params.userId);
    user = await promiseWrap(User.findById.bind(User), [userId]);
    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    if (user.type !== 'carrier') {
      rj.setAll(400, USER_NOT_CARRIER, ERROR_USER_WRONG_TYPE);
      return res.status(rj.status).json(rj);
    }
    if (!user.advanced) {
      rj.setAll(400, USER_NOT_HAVE_ADVANCED, ERROR_USER_NOT_HAVE_ADVANCED);
      rj.set('cars', []);
      return res.status(rj.status).json(rj);
    }
    carrier = await promiseWrap(Carrier.findById.bind(Carrier), [user.advanced]);
    let carsLength = carrier.cars ? carrier.cars.length : 0;
    for (let i = 0, max = carsLength; i < max; i += 1) {
      resultGet = carrierGetCar(carrier.cars[i], fields);
      cars.push(resultGet.car);
      wrongValues = resultGet.wrongValues;
    }

    rj.setDescription(CARS_GOT);
    rj.set('cars', cars);
    rj.set('wrongValue', wrongValues);
    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};