const log = require('../../../libs/log')(module);
const mongoose = require('mongoose');
const User = require('../../../models/user');
const Carrier = require('../../../models/carrier');
const promiseWrap = require('../../../libs/promiseWrap');
const checkObjectId = require('../../libs/checkMongooseObjectId');
const ResponseJson = require('../../libs/response');

const USER_NOT_FOUND = require('../../libs/descriptionConstant').USER_NOT_FOUND;
const CAR_NOT_FOUND = require('../../libs/descriptionConstant').CAR_NOT_FOUND;
const CAR_DELETED = require('../../libs/descriptionConstant').CAR_DELETED;
const USER_NOT_HAVE_ADVANCED = require('../../libs/descriptionConstant').USER_NOT_HAVE_ADVANCED;
const INTERNAL_SERVER_ERROR = require('../../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_NOT_CARRIER = require('../../libs/descriptionConstant').USER_NOT_CARRIER;

const ERROR_USER_NOT_FOUND = require('../../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_CAR_NOT_FOUND = require('../../libs/errorCodeConstant').ERROR_CAR_NOT_FOUND;
const ERROR_INTERNAL_SERVER_ERROR = require('../../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_USER_NOT_HAVE_ADVANCED = require('../../libs/errorCodeConstant').ERROR_USER_NOT_HAVE_ADVANCED;
const ERROR_USER_WRONG_TYPE = require('../../libs/errorCodeConstant').ERROR_USER_WRONG_TYPE;

module.exports = async (req, res) => {
  try {
    let userId;
    let carId;
    let user;
    let carrier;
    let rj = new ResponseJson();

    if (!checkObjectId(req.params.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    if (!checkObjectId(req.params.carId)) {
      rj.setAll(404, CAR_NOT_FOUND, ERROR_CAR_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    userId = mongoose.Types.ObjectId(req.params.userId);
    carId = mongoose.Types.ObjectId(req.params.carId);
    user = await promiseWrap(User.findById.bind(User), [userId]);
    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    if (user.type !== 'carrier') {
      rj.setAll(400, USER_NOT_CARRIER, ERROR_USER_WRONG_TYPE);
      return res.status(rj.status).json(rj);
    }
    if (!user.advanced) {
      rj.setAll(404, USER_NOT_HAVE_ADVANCED, ERROR_USER_NOT_HAVE_ADVANCED);
      return res.status(rj.status).json(rj);
    }

    carrier = await promiseWrap(Carrier.findById.bind(Carrier), [user.advanced]);

    if (!carrier.cars) {
      rj.setAll(404, CAR_NOT_FOUND, ERROR_CAR_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    if (!deleteCar(carrier.cars, carId, rj)) {
      return res.status(rj.status).json(rj);
    }
    await promiseWrap(carrier.save.bind(carrier));
    rj.setDescription(CAR_DELETED);
    res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};

let deleteCar = (cars, carId, rj) => {
  for (let i = 0, max = cars.length; i < max; i += 1) {
    if (cars[i]._id.toString() === carId.toString()) {
      cars.splice(i, 1);
      return true;
    }
  }
  rj.setAll(404, CAR_NOT_FOUND, ERROR_CAR_NOT_FOUND);
  return false;
};