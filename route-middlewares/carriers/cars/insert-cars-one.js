const mongoose = require('mongoose');
const log = require('../../../libs/log')(module);
const User = require('../../../models/user');
const Carrier = require('../../../models/carrier');
const promiseWrap = require('../../../libs/promiseWrap');
const carrierInsertCar = require('../../../services-database/carrier/carrier-inserters/carrier-insert-car');

module.exports = async (req, res) => {
  try {
    let userId;
    let carId;
    let user;
    let carrier;
    let resultInsert = {};
    let carSearched = false;

    try {
      userId = mongoose.Types.ObjectId(req.params.userId);
    } catch (err) {
      log.error(err.message);
      return res.status(404).json({
        status: 404,
        message: 'Not found',
        description: 'This user does not exist',
        errorCode: 9
      });
    }
    try {
      carId = mongoose.Types.ObjectId(req.params.carId);
    } catch (err) {
      log.error(err.message);
      return res.status(404).json({
        status: 404,
        message: 'Not found',
        description: 'This cars does not exist',
        errorCode: 11
      });
    }
    user = await promiseWrap(User.findById.bind(User), [userId]);
    if (!user) {
      return res.status(404).json({
        status: 404,
        message: 'Not found',
        description: 'This user does not exist',
        errorCode: 9
      });
    }
    if (user.type !== 'carrier') {
      return res.status(400).json({
        status: 400,
        message: 'Not found',
        description: 'This user is not a carrier',
        errorCode: 7
      });
    }
    if (!user.advanced) {
      let carrier = new Carrier({
        userId: user._id
      });
      carrier = await promiseWrap(carrier.save.bind(carrier));
      user.advanced = carrier._id;
      await promiseWrap(user.save.bind(user));
    }
    carrier = await promiseWrap(Carrier.findById.bind(Carrier), [user.advanced]);
    carrier.cars.forEach((el, i) => {
      if (el._id.toString() === carId.toString()) {
        let fields = {};
        Object.assign(fields, req.body);
        resultInsert = carrierInsertCar(carrier, fields, i);
        carSearched = true;
      }
    });
    if (!carSearched) {
      return res.status(404).json({
        status: 404,
        message: 'Not found',
        description: 'This cars does not exist',
        errorCode: 11
      });
    }
    await promiseWrap(carrier.save.bind(carrier));
    return res.status(200).json({
      status: 200,
      message: 'OK',
      description: 'Car changed',
      addedValues: resultInsert.addedValues,
      notAddedValues: resultInsert.notAddedValues,
      wrongValues: resultInsert.wrongValues
    });
  } catch(err) {
    log.error(err.message);
    return res.status(500).json({
      status: 500,
      message: 'Internal server error',
      description: 'Internal server error'
    });
  }
};