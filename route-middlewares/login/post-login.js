const User = require('../../models/user');
const log = require('../../libs/log')(module);
const ResponseJson = require('../libs/response');

const USER_SIGN_IN = require('../libs/descriptionConstant').USER_SIGN_IN;
const WRONG_PASSWORD_LOGIN = require('../libs/descriptionConstant').WRONG_PASSWORD_LOGIN;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;

const ERROR_WRONG_PASSWORD_LOGIN = require('../libs/errorCodeConstant').ERROR_WRONG_PASSWORD_LOGIN;
const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;

module.exports = async (req, res) => {
  try {
    let login = req.body.login || '0';
    let password = req.body.password || '0';
    let rj = new ResponseJson();
    let check = await loginUser(login, password, req.session);

    if (check) {
      rj.setDescription(USER_SIGN_IN);
      return res.status(rj.status).json(rj);
    } else {
      rj.setAll(400, WRONG_PASSWORD_LOGIN, ERROR_WRONG_PASSWORD_LOGIN);
      return res.status(rj.status).json(rj);
    }
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};

let loginUser = (login, password, session) => {
  return new Promise((resolve, reject) => {
      User.findOne({phoneNumber: login}, (err, user) => {
        if (err) {
          log.error(err.message);
          return reject(err);
        }
        let check = user && user.checkPassword(password);
        session.user = check ? user._id : null;
        return resolve(check);
      });
  });
};