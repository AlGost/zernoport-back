const User = require('../../models/user');
const promiseWrap = require('../../libs/promiseWrap');
const mongoose = require('mongoose');
const log = require('../../libs/log')(module);
const checkObjectId = require('../libs/checkMongooseObjectId');
const ResponseJson = require('../libs/response');
const insersectionArray = require('../libs/insersection-array');
const userGetData = require('../../services-database/user/user-getters/user-get-data');

const USER_NOT_FOUND = require('../libs/descriptionConstant').USER_NOT_FOUND;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_GOT = require('../libs/descriptionConstant').USER_GOT;

const ERROR_USER_NOT_FOUND = require('../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;

const THIS_USER = require('../../abac-attributes/levels').THIS_USER;

module.exports = async (req, res) => {
  try {
    let userId;
    let user;
    let fields = req.query.fields ? req.query.fields.split(',') : [];
    let canGetFields = ['id', 'firstName', 'lastName', 'patronymic', 'type'];
    let rj = new ResponseJson();
    let resultGet;

    if (req.params.userId !== 'me' && !checkObjectId(req.params.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    userId = req.params.userId === 'me' ? req.user._id : mongoose.Types.ObjectId(req.params.userId);
    user = await promiseWrap(User.findById.bind(User), [userId]);

    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    if (req.levelAccess <= THIS_USER) {
      canGetFields.splice(canGetFields.length, 0, 'email', 'phoneNumber', 'advanced');
    }
    fields = insersectionArray(fields, canGetFields);
    resultGet = userGetData(user, fields);

    rj.setDescription(USER_GOT);
    rj.set('user', resultGet.user);

    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};