const getAll = require('./get-all');
const getOne = require('./get-one');
const putOne = require('./put-one');
const deleteOne = require('./delete-one');

let UsersMiddlewares = function() {
  this.deleteOne = deleteOne;
  this.getAll = getAll;
  this.getOne = getOne;
  this.putOne = putOne;
};

let um = new UsersMiddlewares();

module.exports = um;