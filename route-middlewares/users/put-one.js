const User = require('../../models/user');
const promiseWrap = require('../../libs/promiseWrap');
const mongoose = require('mongoose');
const log = require('../../libs/log')(module);
const checkObjectId = require('../libs/checkMongooseObjectId');
const ResponseJson = require('../libs/response');
const userInsertData = require('../../services-database/user/user-inserters/user-insert-data');

const USER_NOT_FOUND = require('../libs/descriptionConstant').USER_NOT_FOUND;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_CHANGED = require('../libs/descriptionConstant').USER_CHANGED;
const USER_ALREADY_EXISTS = require('../libs/descriptionConstant').USER_ALREADY_EXISTS;

const ERROR_USER_NOT_FOUND = require('../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_USER_ALREADY_EXISTS = require('../libs/errorCodeConstant').ERROR_USER_ALREADY_EXISTS;

const THIS_USER = require('../../abac-attributes/levels').THIS_USER;

module.exports = async (req, res) => {
  try {
    let userId;
    let user;
    let rj = new ResponseJson();
    let resultInsert;
    let resultUnique;
    let reqBody = Object.assign({}, req.body);

    if (req.params.userId !== 'me' && !checkObjectId(req.params.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    userId = req.params.userId === 'me' ? req.user._id : mongoose.Types.ObjectId(req.params.userId);
    user = await promiseWrap(User.findById.bind(User), [userId]);

    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    if (req.levelAccess > THIS_USER) {
      delete reqBody.password;
      delete reqBody.type;
    }

    if (reqBody.phoneNumber === user.phoneNumber) {
      delete reqBody.phoneNumber;
    }
    if (reqBody.email === user.email) {
      delete reqBody.email;
    }

    resultUnique = await checkUnique(reqBody);
    if (!resultUnique.successful) {
      rj.setAll(400, USER_ALREADY_EXISTS, ERROR_USER_ALREADY_EXISTS);
      rj.set('notUniqueFields', resultUnique.notUniqueFields);
      return res.status(rj.status).json(rj);
    }

    resultInsert = userInsertData(user, reqBody);
    await promiseWrap(user.save.bind(user));

    rj.setDescription(USER_CHANGED);
    rj.set('addedValues', resultInsert.addedValues);
    rj.set('notAddedValues', resultInsert.notAddedValues);
    rj.set('wrongValues', resultInsert.wrongValues);

    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};

let checkUnique = function(options) {
  return new Promise(async (resolve, reject) => {
    try {
      let uniqueFields = ['email', 'phoneNumber'];
      let user;
      let search = {};
      let successful = true;
      let notUniqueFields = '';

      for (let i = 0; i < uniqueFields.length; i += 1) {
        search[uniqueFields[i]] = options[uniqueFields[i]];
        if (search[uniqueFields[i]]) {
          user = await promiseWrap(User.findOne.bind(User), [search]);
        }
        if (user) {
          successful = false;
          notUniqueFields += uniqueFields[i] + ' ';
        }
        search = {};
      }
      notUniqueFields = notUniqueFields.substr(0, notUniqueFields.length - 1);

      return resolve({
        successful,
        notUniqueFields
      });
    } catch(err) {
      log.error(err.message);
      return reject(err);
    }
  });
};