const User = require('../../models/user');
const promiseWrap = require('../../libs/promiseWrap');
const log = require('../../libs/log')(module);
const ResponseJson = require('../libs/response');
const userGetData = require('../../services-database/user/user-getters/user-get-data');

const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USERS_GOT = require('../libs/descriptionConstant').USERS_GOT;

const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;

module.exports = async (req, res) => {
  try {
    let rj = new ResponseJson();
    let offset = +req.query.offset || 0;
    let count = +req.query.count || 20;
    let fields = req.query.fields ? req.query.fields.split(',') : [];
    let users;
    let resUsers = [];
    let resultGet = {
      wrongValues: ''
    };
    let options = {
      'limit': count,
      'skip': offset,
      'sort': 'lastName firstName'
    };

    users = await promiseWrap(User.find.bind(User), [{}, {}, options]);
    users.forEach((el) => {
      resultGet = userGetData(el, fields);
      resUsers.push(resultGet.user);
    });

    rj.setDescription(USERS_GOT);
    rj.set('users', resUsers);
    rj.set('wrongValue', resultGet.wrongValues);

    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};