const User = require('../../models/user');
const Carrier = require('../../models/carrier');
const log = require('../../libs/log')(module);
const mongoose = require('mongoose');
const promiseWrap = require('../../libs/promiseWrap');
const checkObjectId = require('../libs/checkMongooseObjectId');
const ResponseJson = require('../libs/response');

const USER_NOT_FOUND = require('../libs/descriptionConstant').USER_NOT_FOUND;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_DELETED = require('../libs/descriptionConstant').USER_DELETED;

const ERROR_USER_NOT_FOUND = require('../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;



module.exports = async (req, res) => {
  try {
    let userId;
    let user;
    let rj = new ResponseJson();

    if (!checkObjectId(req.params.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    userId = mongoose.Types.ObjectId(req.params.userId);
    user = await promiseWrap(User.findById.bind(User), [userId]);
    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    if (user.advanced) {
      await promiseWrap(Carrier.remove.bind(Carrier), [{_id: user.advanced}]);
    }
    await promiseWrap(User.remove.bind(User), [{_id: userId}]);

    rj.setDescription(USER_DELETED);
    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};