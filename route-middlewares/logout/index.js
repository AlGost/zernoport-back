const post = require('./post-logout');

let LogoutMiddlewares = function() {
  this.post = post;
};

module.exports = new LogoutMiddlewares();