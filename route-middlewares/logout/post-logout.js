const log = require('../../libs/log')(module);
const ResponseJson = require('../libs/response');

const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_SIGN_OUT = require('../libs/descriptionConstant').USER_SIGN_OUT;

const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;

module.exports = async (req, res) => {
  try {
    let rj = new ResponseJson();

    req.session.user = null;
    rj.setDescription(USER_SIGN_OUT);
    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};