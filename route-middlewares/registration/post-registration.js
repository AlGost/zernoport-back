const User = require('../../models/user');
const validatorRegistration = require('../../validate').user;
const promiseWrap = require('../../libs/promiseWrap');
const log = require('../../libs/log')(module);
const ResponseJson = require('../libs/response');

const USER_ALREADY_EXISTS = require('../libs/descriptionConstant').USER_ALREADY_EXISTS;
const USER_NOT_VALIDATE = require('../libs/descriptionConstant').USER_NOT_VALIDATE;
const USER_CREATED = require('../libs/descriptionConstant').USER_CREATED;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;

const ERROR_USER_ALREADY_EXISTS = require('../libs/errorCodeConstant').ERROR_USER_ALREADY_EXISTS;
const ERROR_USER_NOT_VALIDATE = require('../libs/errorCodeConstant').ERROR_USER_NOT_VALIDATE;
const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;

module.exports = async (req, res) => {
  try {
    let rj = new ResponseJson();
    let resultValidate;
    let resultUnique;
    let reqBody = Object.assign({}, req.body);

    resultValidate = validatorRegistration(reqBody);
    if (!resultValidate.successful) {
      rj.setAll(400, USER_NOT_VALIDATE, ERROR_USER_NOT_VALIDATE);
      rj.set('invalidFields', resultValidate.invalidFields);
      return res.status(rj.status).json(rj);
    }

    resultUnique = await checkUnique(reqBody);
    if (!resultUnique.successful) {
      rj.setAll(400, USER_ALREADY_EXISTS, ERROR_USER_ALREADY_EXISTS);
      rj.set('invalidFields', resultUnique.notUniqueFields);
      return res.status(rj.status).json(rj);
    }

    reqBody.type = 'carrier';
    await User.createUser(reqBody);

    rj.setDescription(USER_CREATED);
    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};

let checkUnique = function(options) {
  return new Promise(async (resolve, reject) => {
    try {
      let uniqueFields = ['email', 'phoneNumber'];
      let user;
      let search = {};
      let successful = true;
      let notUniqueFields = '';

      for (let i = 0; i < uniqueFields.length; i += 1) {
        search[uniqueFields[i]] = options[uniqueFields[i]];
        if (search[uniqueFields[i]]) {
          user = await promiseWrap(User.findOne.bind(User), [search]);
        }
        if (user) {
          successful = false;
          notUniqueFields += uniqueFields[i] + ' ';
        }
        search = {};
      }
      notUniqueFields = notUniqueFields.substr(0, notUniqueFields.length - 1);

      return resolve({
        successful,
        notUniqueFields
      });
    } catch(err) {
      log.error(err.message);
      return reject(err);
    }
  });
};