const post = require('./post-registration');

let RegistrationMiddlewares = function() {
  this.post = post;
};

module.exports = new RegistrationMiddlewares();