const log = require('../../libs/log')(module);
const Marker = require('../../models/marker');
const promiseWrap = require('../../libs/promiseWrap');
const ResponseJson = require('../libs/response');
const markerGetData = require('../../services-database/marker/marker-getters/marker-get-data');
const insersectionArray = require('../libs/insersection-array');

const MARKERS_GOT = require('../libs/descriptionConstant').MARKERS_GOT;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;

const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;

const ADMIN = require('../../abac-attributes/levels').ADMIN;
const AUTH_USER = require('../../abac-attributes/levels').AUTH_USER;

module.exports = async (req, res) => {
  try {
    let markers;
    let markerLength;
    let resultGet;
    let offset = +req.query.offset || 0;
    let count = +req.query.count || 20;
    let fields = req.query.fields ? req.query.fields.split(',') : [];
    let resMarkers = [];
    let canGetFields = ['id', 'capacity', 'directions', 'typePay', 'dislocation',
      'dislocationMin', 'glonass', 'countCars'];
    let rj = new ResponseJson();
    let searchOptions = {
      'limit': count,
      'skip': offset
    };
    let queryOptions = {
      activated: false
    };

    if (req.levelAccess <= AUTH_USER ) {
      canGetFields.splice(canGetFields.length, 0, 'userId', 'carrierId', 'user');
    }
    if (req.levelAccess <= ADMIN) {
      canGetFields.splice(canGetFields.length, 0, 'activated');
      queryOptions.activated = {$in: [false, true]};
    }

    fields = insersectionArray(fields, canGetFields);
    markers = await promiseWrap(Marker.find.bind(Marker),  [queryOptions, {}, searchOptions]);

    markerLength = markers ? markers.length : 0;
    for (let i = 0; i < markerLength; i += 1) {
      resultGet = await markerGetData(markers[i], fields);
      resMarkers.push(resultGet.marker);
    }
    rj.setDescription(MARKERS_GOT);
    rj.set('markers', resMarkers);
    return res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};