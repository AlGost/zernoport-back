const mongoose = require('mongoose');
const log = require('../../libs/log')(module);
const Marker = require('../../models/marker');
const promiseWrap = require('../../libs/promiseWrap');
const checkObjectId = require('../libs/checkMongooseObjectId');
const ResponseJson = require('../libs/response');
const markerValidator = require('../../validate').marker;
const markerInsertData = require('../../services-database/marker/marker-inserters/marker-insert-data');

const MARKER_NOT_FOUND = require('../libs/descriptionConstant').MARKER_NOT_FOUND;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const MARKER_CHANGED = require('../libs/descriptionConstant').MARKER_CHANGED;

const ERROR_MARKER_NOT_FOUND = require('../libs/errorCodeConstant').ERROR_MARKER_NOT_FOUND;
const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;

const ADMIN = require('../../abac-attributes/levels').ADMIN;

module.exports = async (req, res) => {
  try {
    let rj = new ResponseJson();
    let reqMarker = {
      capacity: req.body.capacity,
      directions: req.body.directions,
      typePay: req.body.typePay,
      dislocation: req.body.dislocation,
      countCars: req.body.countCars,
      lat: req.body.dislocation.lat,
      lng: req.body.dislocation.lng,
      name: req.body.dislocation.name,
      glonass: req.body.glonass,
    };
    let marker;
    let markerId;
    let resultValidate;
    let invalidFields;
    let reqBody = Object.assign({}, req.body);

    resultValidate = markerValidator(reqMarker);

    invalidFields = resultValidate.invalidFields.split(' ');
    invalidFields.forEach((el) => {
      delete reqBody[el];
    });

    if (!checkObjectId(req.params.markerId)) {
      rj.setAll(404, MARKER_NOT_FOUND, ERROR_MARKER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    markerId = mongoose.Types.ObjectId(req.params.markerId);
    marker = await promiseWrap(Marker.findById.bind(Marker), [markerId]);

    if (!marker) {
      rj.setAll(404, MARKER_NOT_FOUND, ERROR_MARKER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    if (req.levelAccess > ADMIN) {
      delete reqBody.activated;
      delete reqBody.userId;
      delete reqBody.carrierId;
    }

    if (Object.keys(reqBody).length !== 0) {
      markerInsertData(marker, reqBody);
    }
    rj.setDescription(MARKER_CHANGED);
    rj.set('invalidFields', invalidFields);

    marker.activated = false;
    await promiseWrap(marker.save.bind(marker));
    res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};