const mongoose = require('mongoose');
const log = require('../../libs/log')(module);
const Marker = require('../../models/marker');
const promiseWrap = require('../../libs/promiseWrap');
const checkObjectId = require('../libs/checkMongooseObjectId');
const ResponseJson = require('../libs/response');
const markerGetData = require('../../services-database/marker/marker-getters/marker-get-data');
const insersectionArray = require('../libs/insersection-array');

const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const MARKER_NOT_FOUND = require('../libs/descriptionConstant').MARKER_NOT_FOUND;
const MARKER_GOT = require('../libs/descriptionConstant').MARKER_GOT;

const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_MARKER_NOT_FOUND = require('../libs/errorCodeConstant').ERROR_MARKER_NOT_FOUND;

module.exports = async (req, res) => {
  try {
    let rj = new ResponseJson();
    let marker;
    let markerId;
    let fields = req.query.fields ? req.query.fields.split(',') : [];
    let resultGet = {};
    let canGetFields = ['id', 'capacity', 'directions', 'typePay', 'dislocation',
      'dislocationMin', 'glonass', 'user', 'userId', 'carrierId'];

    if (!checkObjectId(req.params.markerId)) {
      rj.setAll(404, MARKER_NOT_FOUND, ERROR_MARKER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    markerId = mongoose.Types.ObjectId(req.params.markerId);
    marker = await promiseWrap(Marker.findById.bind(Marker), [markerId]);

    if (!marker) {
      rj.setAll(404, MARKER_NOT_FOUND, ERROR_MARKER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    fields = insersectionArray(fields, canGetFields);
    resultGet = await markerGetData(marker, fields);

    rj.setDescription(MARKER_GOT);
    rj.set('marker', resultGet.marker);
    res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};