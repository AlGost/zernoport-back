const mongoose = require('mongoose');
const log = require('../../libs/log')(module);
const Marker = require('../../models/marker');
const Carrier = require('../../models/carrier');
const promiseWrap = require('../../libs/promiseWrap');
const checkObjectId = require('../libs/checkMongooseObjectId');
const ResponseJson = require('../libs/response');

const MARKER_NOT_FOUND = require('../libs/descriptionConstant').MARKER_NOT_FOUND;
const MARKER_DELETED = require('../libs/descriptionConstant').MARKER_DELETED;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;

const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_MARKER_NOT_FOUND = require('../libs/errorCodeConstant').ERROR_MARKER_NOT_FOUND;


module.exports = async (req, res) => {
  try {
    let rj = new ResponseJson();
    let marker;
    let markerId;
    let carrier;

    if (!checkObjectId(req.params.markerId)) {
      rj.setAll(404, MARKER_NOT_FOUND, ERROR_MARKER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    markerId = mongoose.Types.ObjectId(req.params.markerId);
    marker = await promiseWrap(Marker.findById.bind(Marker), [markerId]);

    if (!marker) {
      rj.setAll(404, MARKER_NOT_FOUND, ERROR_MARKER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }

    carrier = await promiseWrap(Carrier.findById.bind(Carrier), [marker.carrierId]);
    if (carrier) {
      let index;
      carrier.marker.forEach((el, i) => {
        if (el.toString() === markerId.toString()) {
          index = i;
        }
      });
      if (index !== undefined) {
        carrier.marker.splice(index, 1);
      }
      await promiseWrap(carrier.save.bind(carrier));
    }
    await promiseWrap(Marker.deleteOne.bind(Marker), [{_id: markerId}]);
    rj.setDescription(MARKER_DELETED);

    res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};