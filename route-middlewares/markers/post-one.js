const mongoose = require('mongoose');
const log = require('../../libs/log')(module);
const Marker = require('../../models/marker');
const User = require('../../models/user');
const Carrier = require('../../models/carrier');
const promiseWrap = require('../../libs/promiseWrap');
const checkObjectId = require('../libs/checkMongooseObjectId');
const ResponseJson = require('../libs/response');
const markerValidator = require('../../validate').marker;
const markerInsertData = require('../../services-database/marker/marker-inserters/marker-insert-data');

const USER_NOT_FOUND = require('../libs/descriptionConstant').USER_NOT_FOUND;
const INTERNAL_SERVER_ERROR = require('../libs/descriptionConstant').INTERNAL_SERVER_ERROR;
const USER_NOT_CARRIER = require('../libs/descriptionConstant').USER_NOT_CARRIER;
const MARKER_VALIDATE_FAIL = require('../libs/descriptionConstant').MARKER_VALIDATE_FAIL;
const USER_NOT_HAVE_ADVANCED = require('../libs/descriptionConstant').USER_NOT_HAVE_ADVANCED;
const MARKER_CREATED = require('../libs/descriptionConstant').MARKER_CREATED;

const ERROR_USER_NOT_FOUND = require('../libs/errorCodeConstant').ERROR_USER_NOT_FOUND;
const ERROR_INTERNAL_SERVER_ERROR = require('../libs/errorCodeConstant').ERROR_INTERNAL_SERVER_ERROR;
const ERROR_MARKER_VALIDATE_FAIL = require('../libs/errorCodeConstant').ERROR_MARKER_VALIDATE_FAIL;
const ERROR_USER_WRONG_TYPE = require('../libs/errorCodeConstant').ERROR_USER_WRONG_TYPE;
const ERROR_USER_NOT_HAVE_ADVANCED = require('../libs/errorCodeConstant').ERROR_USER_NOT_HAVE_ADVANCED;


module.exports = async (req, res) => {
  try {
    let rj = new ResponseJson();
    let reqMarker = {
      capacity: req.body.capacity,
      directions: req.body.directions,
      typePay: req.body.typePay,
      dislocation: req.body.dislocation,
      lat: req.body.dislocation.lat,
      lng: req.body.dislocation.lng,
      name: req.body.dislocation.name,
      glonass: req.body.glonass,
      countCars: req.body.countCars
    };
    let marker;
    let userId;
    let resultValidate;
    let user;
    let carrier;
    let reqBody = Object.assign({}, req.body);

    resultValidate = markerValidator(reqMarker);

    if (!resultValidate.successful) {
      rj.setAll(400, MARKER_VALIDATE_FAIL, ERROR_MARKER_VALIDATE_FAIL);
      rj.set('invalidFields', resultValidate.invalidFields);
      return res.status(rj.status).json(rj);
    }

    if (!checkObjectId(req.body.userId)) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    userId = mongoose.Types.ObjectId(req.body.userId);
    user = await promiseWrap(User.findById.bind(User), [userId]);

    if (!user) {
      rj.setAll(404, USER_NOT_FOUND, ERROR_USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    if (user.type !== 'carrier') {
      rj.setAll(400, USER_NOT_CARRIER, ERROR_USER_WRONG_TYPE);
      return res.status(rj.status).json(rj);
    }
    if (!user.advanced) {
      rj.setAll(400, USER_NOT_HAVE_ADVANCED, ERROR_USER_NOT_HAVE_ADVANCED);
      return res.status(rj.status).json(rj);
    }

    carrier = await promiseWrap(Carrier.findById.bind(Carrier), [user.advanced]);
    reqBody.carrierId = carrier._id;

    marker = new Marker({});
    markerInsertData(marker, reqBody);
    rj.setDescription(MARKER_CREATED);

    marker = await promiseWrap(marker.save.bind(marker));
    carrier.marker.push(marker._id);
    await promiseWrap(carrier.save.bind(carrier));

    res.status(rj.status).json(rj);
  } catch(err) {
    log.error(err.message);
    let rj = new ResponseJson(500, INTERNAL_SERVER_ERROR, ERROR_INTERNAL_SERVER_ERROR);
    return res.status(rj.status).json(rj);
  }
};