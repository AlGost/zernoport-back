const postOne = require('./post-one');
const getAll = require('./get-all');
const getOne = require('./get-one');
const putOne = require('./put-one');
const deleteOne = require('./delete-one');

let MarkersMiddlewares = function() {
  this.postOne = postOne;
  this.getAll = getAll;
  this.putOne = putOne;
  this.getOne = getOne;
  this.deleteOne = deleteOne;
};

let mm = new MarkersMiddlewares();

module.exports = mm;