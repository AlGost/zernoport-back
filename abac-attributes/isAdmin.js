const User = require('../models/user');
const promiseWrap = require('../libs/promiseWrap');
const log = require('../libs/log')(module);

module.exports = async (req) => {
  try {
    if (req.user) {
      let user = await promiseWrap(User.findById.bind(User), [req.user._id]);
      return user && (user.type === 'admin');
    } else {
      return false;
    }
  } catch(err) {
    log.error(err.message);
    return false;
  }
};