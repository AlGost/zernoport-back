const log = require('../libs/log')(module);

module.exports = (req) => {
  try {
    return !!req.user;
  } catch(err) {
    log.error(err.message);
    return false;
  }
};