const User = require('../models/user');
const log = require('../libs/log')(module);

module.exports = (req) => {
  try {
    return req.params.userId === 'me' || req.params.userId.toString() === req.user._id.toString();
  } catch(err) {
    log.error(err.message);
    return false;
  }
};