const Marker = require('../models/marker');
const promiseWrap = require('../libs/promiseWrap');
const mongoose = require('mongoose');
const log = require('../libs/log')(module);

module.exports = async (req) => {
  try {
    if (req.user) {
      let marker = await promiseWrap(Marker.findById.bind(Marker), [mongoose.Types.ObjectId(req.params.markerId)]);
      return marker && (req.user._id.toString() === marker.userId.toString());
    } else {
      return false;
    }
  } catch(err) {
    log.error(err.message);
    return false;
  }
};