const isAdmin = require('./isAdmin');
const isNotAuth = require('./isNotAuth');
const isThisUser = require('./isThisUser');
const isAuthUser = require('./isAuthUser');
const itMarkerThisUser = require('./it-marker-this-user');

module.exports = {
  isAdmin,
  isNotAuth,
  isThisUser,
  isAuthUser,
  itMarkerThisUser,
};