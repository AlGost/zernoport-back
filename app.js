const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const sessionStore = require('./libs/sessionstore.js');
const config = require('./config');
const loadUser = require('./middleware/loadUser');
const cors = require('cors');

const registration = require('./routes/registration');
const login = require('./routes/login');
const logout = require('./routes/logout');
const carriers = require('./routes/carriers');
const users = require('./routes/users');
const markers = require('./routes/markers');

const app = express();

app.use(cors({ origin: 'localhost:8080'}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser(config.get('session:secret')));
app.use(session({
  secret : config.get('session:secret'),
  key: config.get('session:key'),
  cookie: config.get('session:cookie'),
  store: sessionStore
}));
app.use(loadUser);
app.use(express.static(path.join(__dirname, 'public')));

app.use('/login', login);
app.use('/logout', logout);
app.use('/registration', registration);
app.use('/carriers', carriers);
app.use('/users', users);
app.use('/markers', markers);

app.use('/api/login', login);
app.use('/api/logout', logout);
app.use('/api/registration', registration);
app.use('/api/carriers', carriers);
app.use('/api/users', users);
app.use('/api/markers', markers);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  res.status(404).json({
    status: 404,
    message: 'Not Found',
    description: 'Not Found',
    errorCode: 19
  });
});

// error handler
app.use((err, req, res, next) => {
  res.status(500).json({
    status: 500,
    message: 'Internal server error',
    description: 'Internal server error',
    errorCode: 5
  });
});

module.exports = app;
