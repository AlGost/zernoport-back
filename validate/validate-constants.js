const NAME_VALIDATE = {
  type: 'string',
  minValue: null,
  maxValue: null,
  minLength: 1,
  maxLength: 35,
  regular: /^[А-ЯЁ]?[а-я-()ё']+$/,
  require: true
};
const PATRONYMIC_VALIDATE = {
  type: 'string',
  minValue: null,
  maxValue: null,
  minLength: 1,
  maxLength: 35,
  regular: /^[А-ЯЁ]?[а-я-()ё']+$/,
  require: false
};
const EMAIL_VALIDATE = {
  type: 'string',
  minValue: null,
  maxValue: null,
  minLength: 3,
  maxLength: 255,
  regular: /@/,
  require: false
};
const PHONE_VALIDATE = {
  type: 'string',
  minValue: null,
  maxValue: null,
  minLength: 10,
  maxLength: 20,
  regular: null,
  require: true
};
const PASSWORD_VALIDATE = {
  type: 'string',
  minValue: null,
  maxValue: null,
  minLength: 6,
  maxLength: 32,
  regular: null,
  require: true
};



const MARKER_ARRAY_VALIDATE = {
  type: 'object',
  minValue: null,
  maxValue: null,
  minLength: 1,
  maxLength: null,
  regular: null,
  require: true
};

const DISLOCATION_VALIDATE = {
  type: 'object',
  minValue: null,
  maxValue: null,
  minLength: null,
  maxLength: null,
  regular: null,
  require: true
};

const COORD_VALIDATE = {
  type: 'string',
  minValue: null,
  maxValue: null,
  minLength: 1,
  maxLength: 40,
  regular: /^[0-9]+[\.]?[0-9]*$/,
  require: true
};

const COORD_NAME_VALIDATE = {
  type: 'string',
  minValue: null,
  maxValue: null,
  minLength: 1,
  maxLength: 100,
  regular: null,
  require: true
};

const GLONASS_BOOL_VALIDATE = {
  type: 'boolean',
  minValue: null,
  maxValue: null,
  minLength: null,
  maxLength: null,
  regular: null,
  require: true
};

exports.NAME_VALIDATE = NAME_VALIDATE;
exports.PATRONYMIC_VALIDATE = PATRONYMIC_VALIDATE;
exports.EMAIL_VALIDATE = EMAIL_VALIDATE;
exports.PHONE_VALIDATE = PHONE_VALIDATE;
exports.PASSWORD_VALIDATE = PASSWORD_VALIDATE;

exports.MARKER_ARRAY_VALIDATE = MARKER_ARRAY_VALIDATE;
exports.DISLOCATION_VALIDATE = DISLOCATION_VALIDATE;
exports.COORD_VALIDATE = COORD_VALIDATE;
exports.COORD_NAME_VALIDATE = COORD_NAME_VALIDATE;
exports.GLONASS_BOOL_VALIDATE = GLONASS_BOOL_VALIDATE;
