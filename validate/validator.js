class Validator {
  getResult(object, checkinerFields) {
    let invalidFields = '';
    let successful = true;

    for (let key in checkinerFields) {
      if (checkinerFields.hasOwnProperty(key)) {
        if (!checkinerFields[key](object[key])) {
          invalidFields += key + ' ';
          successful = false;
        }
      }
    }
    invalidFields = invalidFields.substr(0, invalidFields.length - 1);
    return {
      invalidFields,
      successful
    }
  }
}

module.exports = Validator;