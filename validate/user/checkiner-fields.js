const checkiner = require('../checkiner');
const NAME_VALIDATE = require('../validate-constants').NAME_VALIDATE;
const PATRONYMIC_VALIDATE = require('../validate-constants').PATRONYMIC_VALIDATE;
const EMAIL_VALIDATE = require('../validate-constants').EMAIL_VALIDATE;
const PHONE_VALIDATE = require('../validate-constants').PHONE_VALIDATE;
const PASSWORD_VALIDATE = require('../validate-constants').PASSWORD_VALIDATE;


module.exports = {
  firstName: (value) => {
    return checkiner.isValidate(value, NAME_VALIDATE);
  },
  lastName: (value) => {
    return checkiner.isValidate(value, NAME_VALIDATE);
  },
  patronymic: (value) => {
    return checkiner.isValidate(value, PATRONYMIC_VALIDATE);
  },
  email: (value) => {
    return checkiner.isValidate(value, EMAIL_VALIDATE);
  },
  phoneNumber: (value) => {
    return checkiner.isValidate(value, PHONE_VALIDATE);
  },
  password: (value) => {
    return checkiner.isValidate(value, PASSWORD_VALIDATE);
  },
};