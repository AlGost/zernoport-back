const checkinerFields = require('./checkiner-fields');
const Validator = require('../validator');

let val = new Validator();

module.exports = (user) => {
  return val.getResult(user, checkinerFields);
};