const log = require('../libs/log')(module);

class Validator {
  constructor() {
    this.options = {
      type: (type) => {
        return typeof this.value === type;
      },
      minValue: (minValue) => {
        return !(minValue && (minValue > this.value));
      },
      maxValue: (maxValue) => {
        return !(maxValue && (maxValue < this.value));
      },
      minLength: (minLength) => {
        return !(minLength && (minLength > this.value.length));
      },
      maxLength: (maxLength) => {
        return !(maxLength && (maxLength < this.value.length));
      },
      regular: (regular) => {
        return !(regular && !regular.test(this.value));
      },
      require: (require) => {
        return true;
      }
    }
  }
  isValidate(value, options) {
    this.value = value;
    if (value === undefined || value === null) {
      return !options.require;
    }
    let result = true;
    for (let el in options) {
      if (options.hasOwnProperty(el)) {
        if (!this.options[el](options[el])) {
          result = false;
        }
      }
    }
    this.value = undefined;
    return result;
  }
}

module.exports = new Validator();

