const user = require('./user');
const marker = require('./marker');

let Validator = function() {
  this.user = user;
  this.marker = marker;
};

module.exports = new Validator();