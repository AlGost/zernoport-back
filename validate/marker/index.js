//const log = require('../../libs/log')(module);
const checkinerFields = require('./checkiner-fields');
const Validator = require('../validator');

let val = new Validator();

module.exports = (marker) => {
  return val.getResult(marker, checkinerFields);
};