const checkiner = require('../checkiner');
const MARKER_ARRAY_VALIDATE = require('../validate-constants').MARKER_ARRAY_VALIDATE;
const DISLOCATION_VALIDATE = require('../validate-constants').DISLOCATION_VALIDATE;
const COORD_VALIDATE = require('../validate-constants').COORD_VALIDATE;
const COORD_NAME_VALIDATE = require('../validate-constants').COORD_NAME_VALIDATE;
const GLONASS_BOOL_VALIDATE = require('../validate-constants').GLONASS_BOOL_VALIDATE;


module.exports = {
  capacity: (value) => {
    return checkiner.isValidate(value, MARKER_ARRAY_VALIDATE);
  },
  directions: (value) => {
    return checkiner.isValidate(value, MARKER_ARRAY_VALIDATE);
  },
  typePay: (value) => {
    return checkiner.isValidate(value, MARKER_ARRAY_VALIDATE);
  },
  dislocation: (value) => {
    return checkiner.isValidate(value, DISLOCATION_VALIDATE) &&
           checkiner.isValidate(value.lat, COORD_VALIDATE) &&
           checkiner.isValidate(value.lng, COORD_VALIDATE) &&
           checkiner.isValidate(value.name, COORD_NAME_VALIDATE);
  },
  glonass: (value) => {
    return checkiner.isValidate(value, GLONASS_BOOL_VALIDATE);
  },
  countCars: (value) => {
    return typeof value === 'number';
  }
};