const log = require('../libs/log')(module);

const val = require('./checkiner');
const NAME_VALIDATE = require('./validate-constants').NAME_VALIDATE;
const PATRONYMIC_VALIDATE = require('./validate-constants').PATRONYMIC_VALIDATE;
const EMAIL_VALIDATE = require('./validate-constants').EMAIL_VALIDATE;
const PHONE_VALIDATE = require('./validate-constants').PHONE_VALIDATE;
const PASSWORD_VALIDATE = require('./validate-constants').PASSWORD_VALIDATE;

class RegistrationValidator {
  constructor() {
    this.validateValue = ['firstName', 'lastName', 'patronymic', 'email', 'phoneNumber', 'password'];
    this.validator = val;
    this.validators = {
      firstName: (value) => {
        return this.validator.isValidate(value, NAME_VALIDATE);
      },
      lastName: (value) => {
        return this.validator.isValidate(value, NAME_VALIDATE);
      },
      patronymic: (value) => {
        return this.validator.isValidate(value, PATRONYMIC_VALIDATE);
      },
      email: (value) => {
        return this.validator.isValidate(value, EMAIL_VALIDATE);
      },
      phoneNumber: (value) => {
        return this.validator.isValidate(value, PHONE_VALIDATE);
      },
      password: (value) => {
        return this.validator.isValidate(value, PASSWORD_VALIDATE);
      },
    }
  }
  getResult(body) {
    this.body = body;
    let invalidFields = '';
    let successful = true;
    for ( let i = 0; i < this.validateValue.length; i += 1 ) {
      let el = this.validateValue[i];
      if ( !this.body[el] && !this.validators[el](this.body[el]) ) {
        invalidFields += el + ' ';
        successful = false;
      }
    }
    invalidFields = invalidFields.substr(0, invalidFields.length - 1);
    return {
      successful,
      invalidFields
    };
  }
}

module.exports = new RegistrationValidator();