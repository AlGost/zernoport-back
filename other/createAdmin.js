const User = require('../models/user');
const promiseWrap = require('../libs/promiseWrap');


let user = {
  firstName: 'Админ',
  lastName: 'Админов',
  patronymic: 'Админович',
  phoneNumber: '71234567890',
  password: 'muchhardestpasswordintheworld',
  type: 'admin'
};

promiseWrap(User.createUser.bind(User), [user])
  .then((user) => {
    console.log('successful');
  })
  .catch((err) => {
    console.log(err);
  });
