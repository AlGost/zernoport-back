log = require('./log')(module);

const promiseWrap = function(cbFunc, args) {
  return new Promise((resolve, reject) => {
    if (args) {
      cbFunc(...args, (err, data) => {
        if (err) {
          log.error(err.message);
          reject(err);
        }
        resolve(data);
      });
    } else {
      cbFunc((err, data) => {
        if (err) {
          log.error(err.message);
          reject(err);
        }
        resolve(data);
      });
    }
  });
};

module.exports = promiseWrap;