const mongoose = require('./mongoose.js');
//var express = require('express');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

let sessionStore = new MongoStore({mongooseConnection : mongoose.connection})

module.exports = sessionStore;