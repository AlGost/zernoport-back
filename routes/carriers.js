const express = require('express');
const carrierMiddleware = require('../route-middlewares/carriers');
//const log = require('../libs/log')(module);
const isAdmin = require('../abac-attributes').isAdmin;
const isThisUser = require('../abac-attributes').isThisUser;
const checkAttributes = require('../middleware/checkAtributes');

const ADMIN = require('../abac-attributes/levels').ADMIN;
const THIS_USER = require('../abac-attributes/levels').THIS_USER;

const router = express.Router();


router.get('',
  checkAttributes([isAdmin], [ADMIN]),
  carrierMiddleware.getAll);

router.get('/:userId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.getOne);

router.get('/:userId/cars',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.getCarsAll);

router.get('/:userId/drivers',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.getDriversAll);

router.get('/:userId/trailers',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.getTrailersAll);

router.get('/:userId/markers',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.getMarkerAll);

router.put('/:userId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.insertOne);

router.put('/:userId/cars',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.insertCarsAll);

/*router.put('/:userId/cars/:carId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.insertCarsOne);*/

router.put('/:userId/trailers',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.insertTrailersAll);

/*router.put('/:userId/trailers/:trailerId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.insertTrailersOne);*/

router.put('/:userId/drivers',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.insertDriversAll);

/*router.put('/:userId/drivers/:driverId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.insertDriversOne);*/

router.put('/:userId/markers',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.insertMarkersAll);

router.delete('/:userId',
  checkAttributes([isAdmin], [ADMIN]),
  carrierMiddleware.deleteOne);

router.delete('/:userId/cars/:carId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.deleteCarsOne);

router.delete('/:userId/drivers/:driverId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.deleteDriversOne);

router.delete('/:userId/trailers/:trailerId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  carrierMiddleware.deleteTrailersOne);


module.exports = router;