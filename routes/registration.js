const express = require('express');
const registrationMiddleware = require('../route-middlewares/registration');
//const log = require('../libs/log')(module);
const router = express.Router();


router.post('/', registrationMiddleware.post);

module.exports = router;