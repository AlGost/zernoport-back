const express = require('express');
//const log = require('../libs/log')(module);
const logoutMiddleware = require('../route-middlewares/logout');
const router = express.Router();

router.post('', logoutMiddleware.post);

module.exports = router;