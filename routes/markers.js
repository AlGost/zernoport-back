const express = require('express');
const markersMiddleware = require('../route-middlewares/markers');

const checkAttributes = require('../middleware/checkAtributes');
const isAdmin = require('../abac-attributes/').isAdmin;
const isNotAuth = require('../abac-attributes').isNotAuth;
const isAuthUser = require('../abac-attributes').isAuthUser;
const itMarkerThisUser = require('../abac-attributes').itMarkerThisUser;


const ADMIN = require('../abac-attributes/levels').ADMIN;
const NOT_AUTH_USER = require('../abac-attributes/levels').NOT_AUTH_USER;
const AUTH_USER = require('../abac-attributes/levels').AUTH_USER;
const THIS_USER = require('../abac-attributes/levels').THIS_USER;

const log = require('../libs/log')(module);
const router = express.Router();


router.post('/', checkAttributes([isAdmin, isAuthUser, isNotAuth], [ADMIN, AUTH_USER, NOT_AUTH_USER]),
                 markersMiddleware.postOne);

router.get('/', checkAttributes([isAdmin, isAuthUser, isNotAuth], [ADMIN, AUTH_USER, NOT_AUTH_USER]),
                markersMiddleware.getAll
);

router.get('/:markerId', checkAttributes([isAdmin, itMarkerThisUser], [ADMIN, THIS_USER]),
                         markersMiddleware.getOne);

router.put('/:markerId', checkAttributes([isAdmin, itMarkerThisUser], [ADMIN, THIS_USER]),
                         markersMiddleware.putOne);

router.delete('/:markerId', checkAttributes([isAdmin, itMarkerThisUser], [ADMIN, THIS_USER]),
                            markersMiddleware.deleteOne);



module.exports = router;