const express = require('express');
//const log = require('../libs/log')(module);
const usersMiddlewares = require('../route-middlewares/users');
const isAdmin = require('../abac-attributes').isAdmin;
const isThisUser = require('../abac-attributes').isThisUser;
const checkAttributes = require('../middleware/checkAtributes');

const ADMIN = require('../abac-attributes/levels').ADMIN;
const THIS_USER = require('../abac-attributes/levels').THIS_USER;


const router = express.Router();

router.get('',
  checkAttributes([isAdmin], [ADMIN]),
  usersMiddlewares.getAll);

router.get('/:userId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  usersMiddlewares.getOne);

router.put('/:userId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  usersMiddlewares.putOne);

router.delete('/:userId',
  checkAttributes([isAdmin, isThisUser], [ADMIN, THIS_USER]),
  usersMiddlewares.deleteOne);

module.exports = router;