const express = require('express');
//const log = require('../libs/log')(module);
const loginMiddleware = require('../route-middlewares/login');
const router = express.Router();


router.post('', loginMiddleware.post);


module.exports = router;