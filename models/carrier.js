const mongoose = require('../libs/mongoose');
const Schema = mongoose.Schema;

let schema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
    unique: true
  },
  typeCarrier: {
    type: String
  },
  passport: {
    whoGiving: {
      type: String
    },
    dateGiving: {
      type: Date
    },
    codeUnit: {
      type: String
    },
    series: {
      type: String
    },
    numbers: {
      type: String
    },
    dateBirthday: {
      type: Date
    },
    placeOfBirth: {
      type: String
    },
    registration: {
      type: String
    }
  },
  bankCard: {
    numbers: {
      type: String
    },
    ownerName: {
      type: String
    }
  },
  requisitesOfIE: {
    fullName: {
      type: String
    },
    INN: {
      type: String
    },
    OGRN: {
      type: String
    },
    checkingAccount: {
      type: String
    },
    corrAccount: {
      type: String
    },
    BIK: {
      type: String
    },
    bank: {
      type: String
    }
  },
  cars: {
    type: [{
      glonass: {
        type: String
      },
      registerSign: {
        type: String
      },
      VIN: {
        type: String
      },
      model: {
        type: String
      },
      series: {
        type: String
      },
      numbers: {
        type: String
      },
      driver: {
        type: Schema.Types.ObjectId
      },
      trailer: {
        type: Schema.Types.ObjectId
      }
    }]
  },
  trailers: {
    type: [{
      registerSign: {
        type: String
      },
      VIN: {
        type: String
      },
      model: {
        type: String
      },
      series: {
        type: String
      },
      numbers: {
        type: String
      },
      volume: {
        type: String
      },
      car: {
        type: Schema.Types.ObjectId
      }
    }]
  },
  drivers: {
    type: [{
      firstName: {
        type: String
      },
      lastName: {
        type: String
      },
      patronymic: {
        type: String
      },
      phoneNumber: {
        type: String
      },
      passport: {
        whoGiving: {
          type: String
        },
        dateGiving: {
          type: Date
        },
        codeUnit: {
          type: String
        },
        series: {
          type: String
        },
        numbers: {
          type: String
        },
        dateBirthday: {
          type: Date
        },
        placeOfBirth: {
          type: String
        },
        registration: {
          type: String
        }
      },
      bankCard: {
        numbers: {
          type: String
        },
        ownerName: {
          type: String
        }
      },
      car: {
        type: Schema.Types.ObjectId
      }
    }]
  },
  marker: {
    type: [Schema.Types.ObjectId]
  }
}, { usePushEach: true });

module.exports = mongoose.model('Carrier', schema);