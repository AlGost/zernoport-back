const mongoose = require('../libs/mongoose');
const Schema = mongoose.Schema;

let schema = new Schema({
  capacity: {
    type: [Number]
  },
  directions: {
    type: [Number]
  },
  typePay: {
    type: [Number]
  },
  dislocation: {
    type: {
      lat: String,
      lng: String,
      name : String
    }
  },
  glonass: {
    type: Boolean
  },
  countCars: {
    type: Number
  },
  userId: {
    type: Schema.Types.ObjectId
  },
  carrierId: {
    type: Schema.Types.ObjectId
  },
  activated: {
    type: Boolean,
    default: false
  }
}, { usePushEach: true });

module.exports = mongoose.model('Markers', schema);