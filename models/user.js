const crypto = require('crypto');
const log = require('../libs/log.js')(module);

const mongoose = require('../libs/mongoose');
const Schema = mongoose.Schema;

let schema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  patronymic: {
    type: String,
    default: ''
  },
  email: {
    type: String,
    default : ''
  },
  phoneNumber: {
    type: String,
    unique: true,
    required: true
  },
  salt: {
    type: String,
    required: true
  },
  hashedPassword: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  isActivated: {
    type: Boolean,
    default: false
  },
  advanced: {
    type: Schema.Types.ObjectId,
    required: false
  }
}, { usePushEach: true });

schema.virtual('password')
  .set(function(password) {
    this._plainPassword = password;
    this.salt = crypto.randomBytes(32).toString('base64');
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function() {
    return this._plainPassword;
  });

schema.methods.encryptPassword = function(password) {
  return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.methods.checkPassword = function(password) {
  return this.encryptPassword(password) === this.hashedPassword;
};

schema.statics.createUser = function(options) {
  return new Promise((resolve, reject) => {
    let user = new this({
      firstName: options.firstName,
      lastName: options.lastName,
      patronymic: options.patronymic,
      email: options.email,
      phoneNumber: options.phoneNumber,
      password: options.password,
      type: options.type
    });
    user.save((err) => {
      if (err) {
        log.error(err.message);
        reject(err);
      }
      resolve(true);
    });
  });
};

module.exports = mongoose.model('User', schema);

