const ResponseJson = require('../route-middlewares/libs/response');

const FORBIDDEN = require('../route-middlewares/libs/descriptionConstant').FORBIDDEN;
const ERROR_FORBIDDEN = require('../route-middlewares/libs/errorCodeConstant').ERROR_FORBIDDEN;

module.exports = (attributes, levels) => {
  return async (req, res, next) => {
    let successful = false;
    let rj;
    req.levelAccess = 100;
    for (let i = 0, max = attributes.length; i < max; i += 1) {
      if (await attributes[i](req)) {
        req.levelAccess = req.levelAccess < levels[i] ? req.levelAccess : levels[i];
        successful = true;
      }
    }
    if (successful) {
      return next();
    }
    rj = new ResponseJson(403, FORBIDDEN, ERROR_FORBIDDEN);
    return res.status(rj.status).json(rj);
  }
};