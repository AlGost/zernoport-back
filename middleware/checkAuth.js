//const log = require('../libs/log')(module);

module.exports = (req, res, next) => {
  if (!req.session.user) {
    return res.status(401).json({
      status: 401,
      message: 'You are not authorized',
      description: 'You are not authorized',
      errorCode: 2
    })
  } else {
    next();
  }
};