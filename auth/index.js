const User = require('../models/user');
const log = require('../libs/log')(module);

const passport = require('passport');
const LocalStrategy = require('passport-local');

passport.use(new LocalStrategy({
    usernameField: 'login',
    passwordField: 'password'
  },
  (login, password, done) => {
    User.findOne({phoneNumber: login}, (err, user) => {
      if (err) {
        log.error(err.message);
        return done(err);
      }
      if (!user) {
        log.error('Unknown user');
        return done(null, false, { message: 'Unknown user' });
      }

      if (!user.checkPassword(password)) {
        log.error('Incorrect password');
        return done(null, false, { message: 'Incorrect password' });
      }
      return done(null, user);
    });
}));

passport.serializeUser((user, done) => {
  done(null, user._id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err,user) => {
    if (err) done(err);
    done(null, user);
  });
});

module.exports = passport;